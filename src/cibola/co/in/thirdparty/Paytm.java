/**
 *	Paytm.java
 *  Created by Sagar Patidar on Oct 1, 2014, 12:35:57 AM
 *	Copyright � 2014 by Cibola Technologies Pvt. Ltd
 *	All rights reserved.
 */

package cibola.co.in.thirdparty;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import cibola.co.in.General.Common;
import cibola.co.in.General.Constants;
import cibola.co.in.General.MyAccountManager;
import cibola.co.in.NetworkManager.Services.NetworkService;

import com.paytm.pgsdk.PaytmMerchant;
import com.paytm.pgsdk.PaytmOrder;
import com.paytm.pgsdk.PaytmPGService;
import com.paytm.pgsdk.PaytmPaymentTransactionCallback;

public class Paytm {
	
	private MyAccountManager accountManager;
	
	public void execPaytmTrans(final Context context, int amount){
		
		accountManager = new MyAccountManager(context);
		PaytmOrder order = new PaytmOrder(Common.generateTransactionId(), accountManager.getMobileNumber(), 
				String.valueOf(amount), accountManager.getEmail(), accountManager.getMobileNumber());
		
		
		PaytmMerchant merchant = new PaytmMerchant("Cibola15869377551156", 
				"WAP", "Retail", "cibola", "javas", 
				Constants.DOMAIN_NAME+"api/generate_checksum/", 
				Constants.DOMAIN_NAME+"api/verify_checksum/");
		PaytmPGService paytmService = PaytmPGService.getStagingService();	
		paytmService.initialize(order, merchant, null);
		final String PAYTM_DEBUG = "paytm debug";
		
		paytmService.startPaymentTransaction(context, true, true, new PaytmPaymentTransactionCallback() {
			
			@Override
			public void someUIErrorOccurred(String error) {
				Log.d(PAYTM_DEBUG,"someUIErrorOccurred: "+error);
				
			}
			
			@Override
			public void onTransactionSuccess(Bundle bundle) {
				
				Log.d(PAYTM_DEBUG,"onTransactionSuccess: "+bundle.toString());
				int amount = (int) Double.parseDouble(bundle.getString("TXNAMOUNT"));
				int currBal = accountManager.getBalance();
				accountManager.setBalance(currBal+amount);
				Intent intent = new Intent(context, NetworkService.class);
				intent.putExtra(Constants.NETWORK_SERVICE_CODE, Constants.NETWORK_SERVICE_CODE_UPDATE_BALANCE);
				intent.putExtra(Constants.INTENT_UPDATE_BALANCE, amount);
				context.startService(intent);
				Common.showToast(context, "Transaction successful. Rs "+amount+" has been added to your cibola account. Enjoy!");
			}
		
			@Override
			public void onTransactionFailure(String errorMessage, Bundle response) {
				Log.d(PAYTM_DEBUG,"onTransactionFailure: "+errorMessage);
				
			}
			
			@Override
			public void onErrorLoadingWebPage(int errorCode, String errorMsg, String failingUrl) {
				Log.d(PAYTM_DEBUG,"onErrorLoadingWebPage: "+errorMsg);
			}
			
			@Override
			public void networkNotAvailable() {
				Log.d(PAYTM_DEBUG,"networkNotAvailable");
				
			}
			
			@Override
			public void clientAuthenticationFailed(String arg0) {
				Log.d(PAYTM_DEBUG,"clientAuthenticationFailed: "+arg0);
				
			}
		});
		
	}
	
}
