package cibola.co.in.UI;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;
import cibola.co.in.General.Common;
import cibola.co.in.General.Constants;
import cibola.co.in.Interfaces.CustomKeypadListener;
import cibola.co.in.UI.Fragments.KeypadFragment;

public class OneTimePay extends ActionBarActivity implements CustomKeypadListener{

	private TextView buttonSend;
	private TextView buttonAsk;
	private TextView text_amount;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_one_time_pay);
		setUpActionBar();
		getSupportFragmentManager().beginTransaction().replace(R.id.one_time_pay_keypad_group, new KeypadFragment()).commit();
		
		EditText edittext_number = (EditText) findViewById(R.id.one_time_pay_number);
		text_amount = (TextView) findViewById(R.id.one_time_pay_amount);
		TextView text1 = (TextView) findViewById(R.id.one_time_pay_add_amount);
		buttonAsk = (TextView) findViewById(R.id.one_time_pay_ask);
		buttonSend = (TextView) findViewById(R.id.one_time_pay_send);
		
		Common.setTextViewFontStyle(Constants.FONT_ROBOTO_REGULAR, getAssets(), text1, buttonAsk, buttonSend);
		Common.setTextViewFontStyle(Constants.FONT_ROBOTO_LIGHT, getAssets(), text_amount);
		Common.setEditTextFontStyle(Constants.FONT_ROBOTO_REGULAR, getAssets(), edittext_number);
		edittext_number.setInputType(EditorInfo.TYPE_NULL);
		
	}
	
	private void setUpActionBar(){
		ActionBar actionBar = getSupportActionBar();
		View view = this.getLayoutInflater().inflate(R.layout.actionbar_one_time, null);
		TextView title = (TextView) view.findViewById(R.id.actionbar_one_time_title);
		Common.setTextViewFontStyle(Constants.FONT_ROBOTO_REGULAR, getAssets(), title);
		view.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View view) {
				onBackPressed();
			}
		});
		actionBar.setCustomView(view);
		actionBar.setDisplayShowCustomEnabled(true);
		actionBar.setDisplayShowHomeEnabled(false);
		actionBar.setDisplayShowTitleEnabled(false);
		actionBar.setDisplayUseLogoEnabled(false);
		actionBar.setDisplayHomeAsUpEnabled(false);
	}
	
	@Override
	public void onBackPressed(){
		super.onBackPressed();
	}

	@Override
	public void onEnteredValueChange(String currentValue) {
		text_amount.setText(currentValue);
		
	}

}
