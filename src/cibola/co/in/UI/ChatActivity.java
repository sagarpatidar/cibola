package cibola.co.in.UI;

import android.animation.LayoutTransition;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.ViewFlipper;
import cibola.co.in.General.Common;
import cibola.co.in.General.Constants;
import cibola.co.in.General.DialogManager;
import cibola.co.in.General.MyAccountManager;
import cibola.co.in.Graphics.PhotoManager;
import cibola.co.in.Interfaces.TransactionCommentDialogCallback;
import cibola.co.in.NetworkManager.ImageType;
import cibola.co.in.NetworkManager.NetworkConnectionDetector;
import cibola.co.in.SqliteManager.Database;
import cibola.co.in.UI.Fragments.DialogFragment;

public class ChatActivity extends ActionBarActivity implements TransactionCommentDialogCallback{

	static Animation slide_in_left, slide_out_right;
	static Animation slide_in_right, slide_out_left;
	private static ActionBar actionBar;
	private PlaceholderFragment holderFragment;
	private static FragmentManager fManager;
	public static boolean isCommentDialogOpen = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_chat);
		fManager = getSupportFragmentManager();
		if (savedInstanceState == null) {
			holderFragment = new PlaceholderFragment(this);
			fManager.beginTransaction()
			.add(R.id.container, holderFragment).commit();
		}

		slide_in_left = AnimationUtils.loadAnimation(this,
				R.anim.slide_in_left);
		slide_out_right = AnimationUtils.loadAnimation(this,
				R.anim.slide_out_right);
		slide_in_right = AnimationUtils.loadAnimation(this,
				R.anim.slide_in_right);
		slide_out_left = AnimationUtils.loadAnimation(this,
				R.anim.slide_out_left);

		actionBar = getSupportActionBar();
	}


	@Override
	public void onBackPressed() {
		if(holderFragment.send_ask == -1){
			holderFragment.slideMainFromSend();

		}
		else if(holderFragment.send_ask==1){
			holderFragment.slideMainFromAsk();
		}
		else{
			if(isCommentDialogOpen){
				DialogFragment dialogFragment = (DialogFragment) getSupportFragmentManager().findFragmentById(android.R.id.content);
				if(dialogFragment!=null){
					dialogFragment.dismiss();
				}
				else{
					Log.d(Constants.DEBUG,"Dialog Fragment is null");
				}
			}
			else{
				super.onBackPressed();
			}
		}
	}


	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment {

		private ViewFlipper flipper;

		private TextView sendButtonLarge;
		private TextView askButtonLarge;
		private TextView sendButtonSmall;
		private TextView askButtonSmall;
		private EditText askEditText;
		private EditText sendEditText;
		private Context mContext;
		private LinearLayout layout;
		private boolean isNoteAvailable = false;
		public int send_ask = 0;

		private Database db;
		private PhotoManager photoManager;
		private MyAccountManager accountManager;
		private String _contact_number;
		private String _contact_name;
		public PlaceholderFragment(Context c) {
			mContext = c;
			photoManager= new PhotoManager();
			db = new Database(c);
			accountManager = new MyAccountManager(c);
		}

		public PlaceholderFragment() {
			photoManager= new PhotoManager();
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_chat, container,
					false);
			sendButtonLarge = (TextView) rootView.findViewById(R.id.chat_send_button_large);
			askButtonLarge = (TextView) rootView.findViewById(R.id.chat_ask_button_large);
			sendButtonSmall = (TextView) rootView.findViewById(R.id.chat_send_button_small);
			askButtonSmall = (TextView) rootView.findViewById(R.id.chat_ask_button_small);

			askEditText = (EditText) rootView.findViewById(R.id.chat_ask_edittext);
			sendEditText = (EditText) rootView.findViewById(R.id.chat_send_edittext);

			Common.setTextViewFontStyle(Constants.FONT_ROBOTO_REGULAR,mContext.getAssets(), sendButtonLarge,askButtonLarge);
			flipper = (ViewFlipper) rootView.findViewById(R.id.viewFlipper);
			Intent i = ((Activity)mContext).getIntent();
			_contact_name = i.getStringExtra(Constants.CONTACT_NAME);
			_contact_number = i.getStringExtra(Constants.CONTACT_ID);
			int totalChat = db.getTotalChatCount(_contact_number);
			boolean isOnCibola = db.getOnCibolaStatus(_contact_number);
			if(totalChat==0 && !isOnCibola){
				int bgColor = i.getIntExtra(Constants.CONTACT_BACKGROUND, 0);
				layout = (LinearLayout) rootView.findViewById(R.id.fragment_chat_root);
				layout.addView(Common.getCustomNoteView(mContext, getActivity().getLayoutInflater(), "Good News", 
						"Althought <b>"+_contact_name+"</b> is not on Cibola, you still can send him/her money. "+getResources().getString(R.string.custom_note_chat_non_cibola_user_description), 
						R.drawable.icon_custom_note_chat_non_cibola_user, bgColor, onClickListener), 0,
						 new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
				isNoteAvailable = true;
			}
			
			
			boolean fromNotification = i.getBooleanExtra("notification", false);
			if(fromNotification){
				accountManager.setNotificationCount(0);
			}

			askButtonSmall.setOnClickListener(onClickListener);
			askButtonLarge.setOnClickListener(onClickListener);
			sendButtonLarge.setOnClickListener(onClickListener);
			sendButtonSmall.setOnClickListener(onClickListener);
			askEditText.setOnKeyListener(keyListener);
			sendEditText.setOnKeyListener(keyListener);

			markRead(_contact_number);
			setupActionBar();
			return rootView;
		}

		private void setupActionBar(){
			View view = getActivity().getLayoutInflater().inflate(R.layout.actionbar_chat, null);

			TextView actionbarName  = (TextView) view.findViewById(R.id.actionbar_chat_name);
			TextView actionbarMobile = (TextView) view.findViewById(R.id.actionbar_chat_mobile);
			ImageView actionBarBack = (ImageView) view.findViewById(R.id.actionbar_chat_back);
			ImageView actionBarPic = (ImageView) view.findViewById(R.id.actionbar_chat_pic);
			LinearLayout actionBarDetails = (LinearLayout) view.findViewById(R.id.actionbar_user_details);
			
			actionBarPic.setImageBitmap(photoManager.getCibolaUserChatPic(mContext, _contact_number, ImageType.Medium));
			Common.setTextViewFontStyle(Constants.FONT_ROBOTO_REGULAR, getActivity().getAssets(), actionbarMobile, actionbarName);
			actionBarBack.setOnClickListener(onClickListener);
			actionBarPic.setOnClickListener(onClickListener);
			actionBarDetails.setOnClickListener(onClickListener);
			
			actionbarName.setText(_contact_name);
			actionbarMobile.setText(_contact_number);
			actionBar.setDisplayShowHomeEnabled(false);
			actionBar.setDisplayUseLogoEnabled(false);
			actionBar.setHomeButtonEnabled(false);
			actionBar.setDisplayShowTitleEnabled(false);
			actionBar.setCustomView(view);
			actionBar.setDisplayShowCustomEnabled(true);
		}

		private void markRead(final String mobile){
			AsyncTask<Void, Void, Void> async_update = new AsyncTask<Void, Void, Void>() {

				@Override
				protected Void doInBackground(Void... params) {
					db.updateAllread(mobile);
					return null;
				}

			};
			async_update.execute();
		}
		
		
		@TargetApi(Build.VERSION_CODES.HONEYCOMB)
		private void setLayoutTransition(){
			LayoutTransition transition = new LayoutTransition();
			layout.setLayoutTransition(transition);
		}

		private OnClickListener onClickListener = new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				switch (v.getId()) {
				case R.id.actionbar_chat_back:
				{
					getActivity().onBackPressed();
					break;
				}
				case R.id.actionbar_chat_pic:
				{
					getActivity().onBackPressed();
					break;
				}
				case R.id.actionbar_user_details:
				{
					if(Common.isImageExistsOnDisk(_contact_number, ImageType.Medium)){
						Intent intent = new Intent(mContext, ImageActivity.class);
						intent.putExtra(Constants.INTENT_CONTACT_NUMBER, _contact_number);
						intent.putExtra(Constants.INTENT_CONTACT_NAME, _contact_name);
						startActivity(intent);
					}
					else{
						Common.showToast(mContext, "No profile photo.");
					}
					break;
				}
				case R.id.custom_note_close:
				{
					if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.HONEYCOMB){
						setLayoutTransition();
					}
					
					if(isNoteAvailable)
						layout.removeViewAt(0);
					
					break;
				}
				case R.id.chat_ask_button_large:
					if(NetworkConnectionDetector.isConnected(mContext)){
						slideInAsk();
						askEditText.requestFocus();
						Common.showSoftInputKeyboard(mContext, askEditText);	
					}
					else{
						DialogManager.showInternetError(mContext);
					}
					break;
				case R.id.chat_send_button_large:
					if(NetworkConnectionDetector.isConnected(mContext)){
						slideInSend();
						sendEditText.requestFocus();
						Common.showSoftInputKeyboard(mContext, sendEditText);
					}
					else{
						DialogManager.showInternetError(mContext);
					}
					break;
				case R.id.chat_ask_button_small:
					if(NetworkConnectionDetector.isConnected(mContext)){
						String askString = askEditText.getText().toString();			
						if(askString.length()>0){
							int askAmount = Integer.parseInt(askString);
							if (askAmount>0){
								askEditText.setText("");
								DialogFragment d = new DialogFragment(getActivity(),_contact_number, askAmount,true);
								d.show(fManager);
							}
							else{
								Common.showToast(mContext, "Please enter a positive number.");
							}
						}
						else{
							Common.showToast(mContext, "Please enter a positive number.");
						}
					}
					else{
						DialogManager.showInternetError(mContext);
					}
					break;
				case R.id.chat_send_button_small:
					if(NetworkConnectionDetector.isConnected(mContext)){
						String sendString = sendEditText.getText().toString();
						if(sendString.length()>0){
							int sendAmount = Integer.parseInt(sendString);
							if(sendAmount>0){
								sendEditText.setText("");
								DialogFragment d = new DialogFragment(getActivity(), _contact_number, sendAmount,false);
								d.show(fManager);	
							}
							else{
								Common.showToast(mContext, "Please enter a positive number.");
							}
						}
						else{
							Common.showToast(mContext, "Please enter a positive number.");
						}
					}
					else{
						DialogManager.showInternetError(mContext);
					}
					break;
				default:
					break;
				}
			}
		};

		private OnKeyListener keyListener = new OnKeyListener() {

			@Override
			public boolean onKey(View v, int keyCode, KeyEvent event) {
				if ((event.getAction() == KeyEvent.ACTION_UP) &&
						(keyCode == KeyEvent.KEYCODE_ENTER)){
					switch (v.getId()) {
					case R.id.chat_send_edittext:
						sendButtonSmall.performClick();
						return true;
					case R.id.chat_ask_edittext:
						askButtonSmall.performClick();
						return true;
					default:
						break;
					}
				}
				return false;
			}
		};

		public void slideInAsk(){
			flipper.setInAnimation(slide_in_right);
			flipper.setOutAnimation(slide_out_left);
			flipper.showPrevious();
			send_ask = 1;
		}
		public void slideInSend(){
			flipper.setInAnimation(slide_in_left);
			flipper.setOutAnimation(slide_out_right);
			flipper.showNext();
			send_ask = -1;
		}
		public void slideMainFromAsk(){
			Common.hideKeyBoardFromEditText(askEditText, mContext);
			flipper.setInAnimation(slide_in_left);
			flipper.setOutAnimation(slide_out_right);
			flipper.showNext();
			send_ask = 0;
		}
		public void slideMainFromSend(){
			Common.hideKeyBoardFromEditText(sendEditText, mContext);
			flipper.setInAnimation(slide_in_right);
			flipper.setOutAnimation(slide_out_left);
			flipper.showPrevious();
			send_ask = 0;
		}



	}

	@Override
	public void onCommentDialogDismiss() {
		onBackPressed();

	}



}
