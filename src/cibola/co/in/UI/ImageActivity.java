package cibola.co.in.UI;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import cibola.co.in.General.Common;
import cibola.co.in.General.Constants;
import cibola.co.in.Graphics.PhotoManager;
import cibola.co.in.NetworkManager.ImageType;
import cibola.co.in.NetworkManager.Services.NetworkService;

public class ImageActivity extends ActionBarActivity {

	private ProgressBar loader;
	private ImageView image;
	private PhotoManager photoManager;
	private MyReceiver downloadNotificationReceiver;
	private Bitmap bit = null;
	private String contactNumber;
	private String contactName;
	private int screenWidth;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_image);
		image = (ImageView) findViewById(R.id.image_view);
		loader = (ProgressBar) findViewById(R.id.image_loading);
		photoManager = new PhotoManager();
		this.downloadNotificationReceiver = new MyReceiver();
		IntentFilter filter = new IntentFilter(Constants.INTENT_LARGE_IMAGE_DOWNLOAD_COMPLETE);
		registerReceiver(downloadNotificationReceiver, filter);
		
		contactNumber = getIntent().getStringExtra(Constants.INTENT_CONTACT_NUMBER);
		contactName = getIntent().getStringExtra(Constants.INTENT_CONTACT_NAME);
		if(Common.isImageExistsOnDisk(contactNumber, ImageType.Large)){
			bit =  photoManager.getCibolaUserPic(ImageActivity.this, contactNumber, "0", ImageType.Large);
			loader.setVisibility(View.GONE);
		}
		else{
			bit =  photoManager.getCibolaUserPic(ImageActivity.this, contactNumber, "0", ImageType.Medium);
			Intent intent = new Intent(this,NetworkService.class);
			intent.putExtra(Constants.NETWORK_SERVICE_CODE, Constants.NETWORK_SERVICE_CODE_DOWNLOAD_LARGE);
			intent.putExtra(Constants.INTENT_CONTACT_NUMBER, contactNumber);
			startService(intent);
		}
		
		
		final RelativeLayout root = (RelativeLayout) findViewById(R.id.activity_image_root);
		ViewTreeObserver vto = root.getRootView().getViewTreeObserver();
		vto.addOnGlobalLayoutListener(new OnGlobalLayoutListener() {
			
			@Override
			public void onGlobalLayout() {
				root.getViewTreeObserver().removeGlobalOnLayoutListener(this);
				screenWidth = root.getMeasuredWidth();
				
				Bitmap bitmap = Bitmap.createScaledBitmap(bit, screenWidth, screenWidth, true);
				image.setImageBitmap(bitmap);
			}
		})	;
		
		Common.setupActionBar(getSupportActionBar());
		getSupportActionBar().setTitle(contactName);
		getSupportActionBar().setDisplayShowTitleEnabled(true);


		int titleId = getResources().getIdentifier("action_bar_title", "id",
            "android");
		TextView actionBarTitle = (TextView) findViewById(titleId);
		actionBarTitle.setTextColor(getResources().getColor(R.color.white));
		Common.setTextViewFontStyle(Constants.FONT_ROBOTO_REGULAR, getAssets(), actionBarTitle);
		actionBarTitle.setText(contactName);

	}

	@Override
	public boolean onOptionsItemSelected(final MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			onBackPressed();
			return true;
		default:
			break;
		}
		return super.onOptionsItemSelected(item);
	}
	
	@Override
	public void onBackPressed(){
		super.onBackPressed();
	}
	
	@Override
	public void onPause(){
		super.onPause();
		unregisterReceiver(downloadNotificationReceiver);
	}
	
	@Override
	public void onResume(){	
		super.onResume();
		IntentFilter filter = new IntentFilter(Constants.INTENT_LARGE_IMAGE_DOWNLOAD_COMPLETE);
		registerReceiver(downloadNotificationReceiver, filter);
	}
	
	private class MyReceiver extends BroadcastReceiver{

		@Override
		public void onReceive(Context context, Intent intent) {
			if(intent.getAction()==Constants.INTENT_LARGE_IMAGE_DOWNLOAD_COMPLETE){
				if(intent.getStringExtra(Constants.INTENT_CONTACT_NUMBER).equals(contactNumber)){
					bit =  photoManager.getCibolaUserPic(ImageActivity.this, contactNumber, "0", ImageType.Large);
					Bitmap bitmap = Bitmap.createScaledBitmap(bit, screenWidth, screenWidth, true);
					image.setImageBitmap(bitmap);
					loader.setVisibility(View.GONE);
				}
			}
			
		}

	}
	
}
