/**
 *	RefundToBank.java
 *  Created by Sagar Patidar on 13-Aug-2014, 12:06:23 AM
 *	Copyright � 2014 by Cibola Pvt. Ltd
 *	All rights reserved.
 */

package cibola.co.in.UI;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import cibola.co.in.General.Common;
import cibola.co.in.General.Constants;
import cibola.co.in.Interfaces.RefundAccountListCallBack;
import cibola.co.in.SqliteManager.Database;

public class RefundToBank extends ActionBarActivity implements RefundAccountListCallBack{

	
	public static String GET_ACCOUNT_DETAILS_BROADCAST_ACTION = "accountDetailsAction";
	
	private EditText amount;
	private TextView submit;
	private TextView title;
	private Database db;
	
	private boolean isAccountListFragmentShown ;
	private String _accountName;
	private String _accountNumber;
	private String _accountIFSC;

	@Override
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.fragment_refund_to_bank);

		TextView text1= (TextView) findViewById(R.id.text1);
		title = (TextView) findViewById(R.id.text2);
		
		submit= (TextView) findViewById(R.id.refund_submit);
		amount = (EditText) findViewById(R.id.refund_amount);

		Common.setTextViewFontStyle(Constants.FONT_ROBOTO_REGULAR,getAssets(), text1, title, submit);
		Common.setEditTextFontStyle(Constants.FONT_ROBOTO_REGULAR, getAssets(), amount);
		setUpActionBar();
		
		submit.setOnClickListener(onClickListener);

		db = new Database(this);
		int totalAccountsAvailable = db.getTotalAccountsCount();
		if(totalAccountsAvailable>0){
			getSupportFragmentManager().beginTransaction().add(R.id.refund_account_container,new AccountsList()).commit();
			submit.setText("Send");
			title.setText("Select account");
			isAccountListFragmentShown = true;
		}
		else{
			getSupportFragmentManager().beginTransaction().add(R.id.refund_account_container,new NewAccount()).commit();
			submit.setText("Add and Proceed");
			title.setText("Account Details");
			isAccountListFragmentShown = false;
		}
	}
	
	private OnClickListener onClickListener = new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.refund_submit:
			{
				Intent intent = new Intent(GET_ACCOUNT_DETAILS_BROADCAST_ACTION);
				sendBroadcast(intent);
				break;
			}
			default:
				break;
			}
			
		}
	};

	private void setUpActionBar(){
		ActionBar actionBar = getSupportActionBar();
		View view = this.getLayoutInflater().inflate(R.layout.actionbar_refund, null);
		TextView title = (TextView) view.findViewById(R.id.actionbar_refund_title);
		Common.setTextViewFontStyle(Constants.FONT_ROBOTO_REGULAR, getAssets(), title);
		view.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				onBackPressed();
			}
		});
		actionBar.setCustomView(view);
		actionBar.setDisplayShowCustomEnabled(true);
		actionBar.setDisplayShowHomeEnabled(false);
		actionBar.setDisplayShowTitleEnabled(false);
		actionBar.setDisplayUseLogoEnabled(false);
		actionBar.setDisplayHomeAsUpEnabled(false);
	}

	@Override
	public void onBackPressed(){
		super.onBackPressed();
		overridePendingTransition(R.anim.activity_slide_in_left, R.anim.activity_slide_out_left);
	}

	private class NewAccount extends Fragment{

		private EditText accountName;
		private EditText accountNumber;
		private EditText accountIFSC;
		private MyReceiver receiver;
		private RefundAccountListCallBack callBack;
		
		public NewAccount(){
		}

		@Override
		public void onCreate(Bundle savedInstanceState){
			super.onCreate(savedInstanceState);
			try{
				callBack = (RefundAccountListCallBack) getActivity();
			}
			catch(Exception e){
				e.printStackTrace();
			}
			
			receiver = new MyReceiver();
			IntentFilter filter = new IntentFilter(GET_ACCOUNT_DETAILS_BROADCAST_ACTION);
			registerReceiver(receiver, filter);
		}
		
		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_refund_add_new_account, container,false);
			TextView text1 = (TextView) rootView.findViewById(R.id.text1);
			TextView text2 = (TextView) rootView.findViewById(R.id.text2);
			TextView text3 = (TextView) rootView.findViewById(R.id.text3);
			accountName = (EditText) rootView.findViewById(R.id.refund_account_name);
			accountNumber = (EditText) rootView.findViewById(R.id.refund_account_number);
			accountIFSC = (EditText) rootView.findViewById(R.id.refund_account_ifsc);

			Common.setTextViewFontStyle(Constants.FONT_ROBOTO_REGULAR,getActivity().getAssets(), text1, text2, text3);
			Common.setEditTextFontStyle(Constants.FONT_ROBOTO_REGULAR, getAssets(), accountIFSC, accountName, accountNumber);
			return rootView;
		}
		
		@Override
		public void onPause(){
			super.onPause();
			unregisterReceiver(receiver);
		}
		
		@Override
		public void onResume(){	
			super.onResume();
			IntentFilter filter = new IntentFilter(GET_ACCOUNT_DETAILS_BROADCAST_ACTION);
			registerReceiver(receiver, filter);
		}
		
		private class MyReceiver extends BroadcastReceiver{

			@Override
			public void onReceive(Context context, Intent intent) {
				System.out.println("received");
				String name = accountName.getText().toString();
				String number = accountName.getText().toString();
				String ifsc = accountName.getText().toString();
				if(name.length()>0 && number.length()>0 && ifsc.length()>0){
					callBack.extractAccountDetails(name, number, ifsc);
				}
				else{
					Common.showToast(getApplicationContext(), "Please fill complete details.");
				}
			}

		}

	}

	private class AccountsList extends Fragment{

		public AccountsList(){
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_refund_accounts_list, container,false);
			return rootView;
		}

	}

	@Override
	public void onClickAddAccount() {
		FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
		ft.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left);
		ft.replace(R.id.refund_account_container,new NewAccount()).commit();
		submit.setText("Add and Proceed");
		title.setText("Account Details");
		isAccountListFragmentShown = false;
	}

	@Override
	public void extractAccountDetails(String accName, String accNumber, String accIFSC) {
		System.out.println(accName+"$"+accNumber+"$"+accIFSC);
		_accountName = accName;
		_accountNumber = accNumber;
		_accountIFSC = accIFSC;
	}

	

}
