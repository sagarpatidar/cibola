/**
 *	ApplicationOpenerActivity.java
 *  Created by Sagar Patidar on 01-Sep-2014, 9:41:33 PM
 *	Copyright ? 2014 by Cibola Technologies Pvt. Ltd
 *	All rights reserved.
 */

package cibola.co.in.UI;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import cibola.co.in.General.MyAccountManager;
import cibola.co.in.NetworkManager.NetworkConnectionDetector;

import com.crashlytics.android.Crashlytics;

public class ApplicationOpenerActivity extends Activity{
	
	@Override
	public void onCreate(Bundle b){
	    
		super.onCreate(b);
		if(NetworkConnectionDetector.isConnected(this)){
			//Crashlytics.start(this);
		}
		
	    MyAccountManager accountManager = new MyAccountManager(this);
	    	    
	    if(!accountManager.isInitialized()){
	    	startActivity(new Intent(this, LoginActivity.class));
	    }
	    else{
	    	if(accountManager.getPwdSetStatus()){
		    	startActivity(new Intent(this, LoginActivity.class));
		    }
	    	else{
		    	startActivity(new Intent(this, MainActivity.class));
		    }
	    }
	    finish();
	}	
}
