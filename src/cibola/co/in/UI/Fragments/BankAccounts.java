/**
 *	BankAccounts.java
 *  Created by Sagar Patidar on 13-Aug-2014, 11:31:27 PM
 *	Copyright � 2014 by Cibola Pvt. Ltd
 *	All rights reserved.
 */

package cibola.co.in.UI.Fragments;

import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.CursorLoader;
import android.support.v4.widget.CursorAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import cibola.co.in.General.Common;
import cibola.co.in.General.Constants;
import cibola.co.in.Interfaces.EditAccountCallbacks;
import cibola.co.in.SqliteManager.DataProvider;
import cibola.co.in.SqliteManager.Database;
import cibola.co.in.SqliteManager.Tables;
import cibola.co.in.UI.R;

public class BankAccounts extends ListFragment implements LoaderCallbacks<Cursor> {


	private BankAccountsAdapter adapter;
	private EditAccountCallbacks callBacks;
	private Database db;
	
	
	@Override
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		callBacks = (EditAccountCallbacks) getActivity();
		db = new Database(getActivity());
		adapter = new BankAccountsAdapter(getActivity(), null, false);
	}	
	
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		getLoaderManager().initLoader(0, null, this);
		ListView lv  = getListView();
		lv.setVerticalScrollBarEnabled(false);
		lv.setHorizontalScrollBarEnabled(false);
		
		View emptyView = getLayoutInflater(savedInstanceState).inflate(R.layout.fragment_empty_accounts, null);
		TextView text01 = (TextView) emptyView.findViewById(R.id.text01);
		Common.setTextViewFontStyle(Constants.FONT_ROBOTO_REGULAR, getActivity().getAssets(), text01);
		ViewGroup viewGroup = (ViewGroup) lv.getParent();
		viewGroup.addView(emptyView);
		lv.setEmptyView(emptyView);
		setListAdapter(adapter);
	}    
	
	@Override
	public void onLoadFinished(android.support.v4.content.Loader<Cursor> arg0,
			Cursor arg1) {
		adapter.swapCursor(arg1);
	}

	@Override
	public void onLoaderReset(android.support.v4.content.Loader<Cursor> arg0) {
		adapter.swapCursor(null);
	}

	@Override
	public android.support.v4.content.Loader<Cursor> onCreateLoader(int arg0,
			Bundle arg1) {
		CursorLoader loader = new CursorLoader(getActivity(), DataProvider.URI_BANK_ACCOUNTS, null, null, null, null);
		return loader;
	}
	
	private class BankAccountsAdapter extends CursorAdapter
	{
		private LayoutInflater li;
		public BankAccountsAdapter(Context context, Cursor c, boolean autoRequery) {
			super(context, c, autoRequery);
			mContext = context;
			li = LayoutInflater.from(context);
		}
		
		private class ViewHolder{
			private TextView accountTitle;
			private TextView accountNumber;
			private ImageView editAccount;
			private ImageView removeAccount;
			public ViewHolder(View v){
				accountTitle = (TextView) v.findViewById(R.id.row_bank_account_title);
				accountNumber = (TextView) v.findViewById(R.id.row_bank_account_acc_no);
				editAccount = (ImageView) v.findViewById(R.id.row_bank_account_edit);
				removeAccount = (ImageView) v.findViewById(R.id.row_bank_account_remove);
				Common.setTextViewFontStyle(Constants.FONT_ROBOTO_REGULAR,mContext.getAssets(), accountNumber, accountTitle);
			}
		}
		
		@Override
		public void bindView(View view, Context context,final Cursor cursor) {
			
			ViewHolder holder = (ViewHolder) view.getTag();
			final String _id = cursor.getString(cursor.getColumnIndex(Tables.account_number));
			final String _name = cursor.getString(cursor.getColumnIndex(Tables.account_name));
			final String _ifsc = cursor.getString(cursor.getColumnIndex(Tables.account_ifsc));
			holder.accountNumber.setText(cursor.getString(cursor.getColumnIndex(Tables.account_number)));
			 
			holder.removeAccount.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					db.removeAccount(_id);
				}
			});
			
			holder.editAccount.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View arg0) {
					callBacks.onAccountEditClicked(_id, _name, _ifsc);
				}
			});
			
		}
		
		@Override
		public View newView(Context context, Cursor cursor, ViewGroup parent) {
			ViewHolder holder;
			View view;
			view = li.inflate(R.layout.row_bank_account, parent, false);
			holder = new ViewHolder(view);
			view.setTag(holder);
			return view;
		}

	}

}
