/**
 *	ChatFragment.java
 *  Created by Sagar Patidar on 05-Aug-2014, 1:17:14 AM
 *	Copyright � 2014 by Cibola Pvt. Ltd
 *	All rights reserved.
 */

package cibola.co.in.UI.Fragments;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.CursorLoader;
import android.support.v4.widget.CursorAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;
import cibola.co.in.General.CircularImageView;
import cibola.co.in.General.Common;
import cibola.co.in.General.Constants;
import cibola.co.in.Graphics.PhotoManager;
import cibola.co.in.Interfaces.TransactionCommentDialogCallback;
import cibola.co.in.NetworkManager.ImageType;
import cibola.co.in.SqliteManager.DataProvider;
import cibola.co.in.SqliteManager.Database;
import cibola.co.in.SqliteManager.Tables;
import cibola.co.in.UI.R;

public class ChatFragment extends ListFragment implements LoaderCallbacks<Cursor>, TransactionCommentDialogCallback {
	

	private ChatAdapter adapter;
	private String contact_id;
	private String contact_name;
	private Bitmap contact_pic;
	private PhotoManager photoManager;
	private Database db;
	private MyReceiver receiver;
	
//	private ImageView loadButton;
//	private View loadmoreView;
//	private final int LOAD_LIMIT = 15;
//	private int totalChatCount = 0;
//	private int loadedChatCount = 0;
//	private int lastCount = -1;
	@Override
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		Intent intent = getActivity().getIntent();
		contact_id = intent.getStringExtra(Constants.CONTACT_ID);
		contact_name = intent.getStringExtra(Constants.CONTACT_NAME);
		photoManager = new PhotoManager();
		contact_pic = photoManager.getCibolaUserChatPic(getActivity(), contact_id, ImageType.Medium);
		adapter = new ChatAdapter(getActivity(), null, false);
		receiver = new MyReceiver();
		db = new Database(getActivity());
//		totalChatCount = db.getTotalChatCount(contact_id);
//		loadMore();
	}
	
	
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		getLoaderManager().initLoader(0, null, this);
		ListView lv  = getListView();
		lv.setBackgroundColor(getResources().getColor(R.color.chat_fragment_background));
		lv.setDividerHeight(0);
		lv.setPadding((int)getResources().getDimension(R.dimen.chat_fragment_padding_horizontal), 0, 
				(int)getResources().getDimension(R.dimen.chat_fragment_padding_horizontal), 0);
		
		lv.setVerticalScrollBarEnabled(false);
		lv.setHorizontalScrollBarEnabled(false);
		
//		loadmoreView =  getLayoutInflater(savedInstanceState).inflate(R.layout.layout_load_more, null);
//		loadButton = (ImageView) loadmoreView.findViewById(R.id.chat_load_more);
//		
//		if(totalChatCount>LOAD_LIMIT){
//			lv.addHeaderView(loadmoreView);
//			loadButton.setOnClickListener(new View.OnClickListener() {
//				
//				@Override
//				public void onClick(View arg0) {
//					loadMore();
//				}
//			});
//		}
		
		lv.setStackFromBottom(true);
		setListAdapter(adapter);
		System.out.println("onActivityCreated");
		setToEnd();
	}   
	
	/*private void loadMore(){
		if(totalChatCount<=LOAD_LIMIT){
			loadedChatCount = totalChatCount;
			//loadmoreView.setVisibility(View.GONE);
		}
		else{
			lastCount = loadedChatCount;
			if(totalChatCount>(LOAD_LIMIT+loadedChatCount)){
				loadedChatCount += LOAD_LIMIT;
			}
			else{
				loadedChatCount = totalChatCount;
				loadmoreView.setVisibility(View.GONE);
			}
			getLoaderManager().restartLoader(0, null, this);
		}
	}*/
	
	private void setToEnd(){
		getListView().setTranscriptMode(ListView.TRANSCRIPT_MODE_ALWAYS_SCROLL);
	}
	
	private class MyReceiver extends BroadcastReceiver{

		@Override
		public void onReceive(Context context, Intent intent) {
			setToEnd();
		}
	}
	
	
	@Override
	public void onPause(){
		super.onPause();
		getActivity().unregisterReceiver(receiver);
	}
	
	@Override
	public void onResume(){
		super.onResume();
		IntentFilter filter = new IntentFilter(Constants.INTENT_COMMENT_DIALOG_DISMISSED);
		getActivity().registerReceiver(receiver, filter);
	}
	
	@Override
	public void onCommentDialogDismiss() {
		getListView().setStackFromBottom(true);
	}
	
	@Override
	public void onLoadFinished(android.support.v4.content.Loader<Cursor> arg0,
			Cursor arg1) {
		adapter.swapCursor(arg1);
		
	}

	@Override
	public void onLoaderReset(android.support.v4.content.Loader<Cursor> arg0) {
		adapter.swapCursor(null);
	}

	@Override
	public android.support.v4.content.Loader<Cursor> onCreateLoader(int arg0,
			Bundle arg1) {
	//	if(totalChatCount<LOAD_LIMIT){
			return new CursorLoader(getActivity(), DataProvider.URI_TRANSACTIONS, null, 
					Tables.trans_opponent_id+"=?", new String[]{contact_id}, null);
/*		}
		else{
			return new CursorLoader(getActivity(), DataProvider.URI_TRANSACTIONS, null, 
					Tables.trans_opponent_id+"=?", new String[]{contact_id}, Tables.trans_timestamp+" ASC LIMIT "+loadedChatCount+" OFFSET "+(totalChatCount-loadedChatCount));
		}*/
		
	}
	
	
	private class ChatAdapter extends CursorAdapter
	{
		private Context mContext;
		private LayoutInflater li;
		private DateFormat date;
		private DateFormat time;
		private Bitmap self;
		
		public ChatAdapter(Context context, Cursor c, boolean autoRequery) {
			super(context, c, autoRequery);
			mContext = context;
			li = LayoutInflater.from(context);
			date =  SimpleDateFormat.getDateInstance();
			
			date.setTimeZone(TimeZone.getTimeZone("GMT+05:30"));

			time = SimpleDateFormat.getTimeInstance(SimpleDateFormat.SHORT);
			time.setTimeZone(TimeZone.getTimeZone("GMT+05:30"));
		
			self = photoManager.getSelfChatPic(mContext); 
		}
		
		private class ViewHolderLeftWithComment{
			private TextView title;
			private TextView amount;
			private TextView comment;
			private TextView time;
			private CircularImageView profile_pic;
			public ViewHolderLeftWithComment(View v){
				title = (TextView) v.findViewById(R.id.row_chat_left_with_comment_send_request_title);
				amount = (TextView) v.findViewById(R.id.row_chat_left_with_comment_send_request_amount);
				comment = (TextView) v.findViewById(R.id.row_chat_left_with_comment_comment);
				time = (TextView) v.findViewById(R.id.row_chat_left_with_comment_time);
				profile_pic = (CircularImageView) v.findViewById(R.id.row_chat_left_with_comment_profile_pic);
				Common.setTextViewFontStyle(Constants.FONT_ROBOTO_REGULAR,mContext.getAssets(), title,amount,comment,time);
			}
		}
		
		private class ViewHolderRightWithComment{
			private TextView title;
			private TextView amount;
			private TextView comment;
			private TextView time;
			private CircularImageView profile_pic;
			public ViewHolderRightWithComment(View v){
				title = (TextView) v.findViewById(R.id.row_chat_right_with_comment_send_request_title);
				amount = (TextView) v.findViewById(R.id.row_chat_right_with_comment_send_request_amount);
				comment = (TextView) v.findViewById(R.id.row_chat_right_with_comment_comment);
				time = (TextView) v.findViewById(R.id.row_chat_right_with_comment_time);
				profile_pic = (CircularImageView) v.findViewById(R.id.row_chat_right_with_comment_profile_pic);
				profile_pic.setImageBitmap(self);
				Common.setTextViewFontStyle(Constants.FONT_ROBOTO_REGULAR,mContext.getAssets(), title,amount,comment,time);
			}
		}
		
		private class ViewHolderLeftWithoutComment{
			private TextView title;
			private TextView amount;
			private TextView time;
			private CircularImageView profile_pic;
			public ViewHolderLeftWithoutComment(View v){
				title = (TextView) v.findViewById(R.id.row_chat_left_without_comment_send_request_title);
				amount = (TextView) v.findViewById(R.id.row_chat_left_without_comment_send_request_amount);
				time = (TextView) v.findViewById(R.id.row_chat_left_without_comment_time);
				profile_pic = (CircularImageView) v.findViewById(R.id.row_chat_left_without_comment_profile_pic);
				Common.setTextViewFontStyle(Constants.FONT_ROBOTO_REGULAR,mContext.getAssets(), title, amount, time);
			}
		}
		
		private class ViewHolderRightWithoutComment{
			private TextView title;
			private TextView amount;
			private TextView time;
			private CircularImageView profile_pic;
			public ViewHolderRightWithoutComment(View v){
				title = (TextView) v.findViewById(R.id.row_chat_right_without_comment_send_request_title);
				amount = (TextView) v.findViewById(R.id.row_chat_right_without_comment_send_request_amount);
				time = (TextView) v.findViewById(R.id.row_chat_right_without_comment_time);
				profile_pic = (CircularImageView) v.findViewById(R.id.row_chat_right_without_comment_profile_pic);
				profile_pic.setImageBitmap(self);
				Common.setTextViewFontStyle(Constants.FONT_ROBOTO_REGULAR,mContext.getAssets(), title, amount, time);
			}
		}
		
		@Override
		public int getItemViewType(int position) {
			Cursor cursor = (Cursor) getItem(position);
			String comment = cursor.getString(cursor.getColumnIndex(Tables.trans_comment));
			if(comment!=null){
				return cursor.getInt(cursor.getColumnIndex(Tables.trans_to_from));
			}
			else{
				return cursor.getInt(cursor.getColumnIndex(Tables.trans_to_from))+2;
			}
		}

		@Override
		public int getViewTypeCount() {
			return 4;
		}

		@Override
		public void bindView(View view, Context context, Cursor cursor) {
			int status = cursor.getInt(cursor.getColumnIndex(Tables.trans_status));
			if(view.getTag() instanceof ViewHolderRightWithComment){
				ViewHolderRightWithComment holder = (ViewHolderRightWithComment) view.getTag();
				if(cursor.getInt(cursor.getColumnIndex(Tables.trans_send_req))==0){
					holder.title.setText("Sent");
				}
				else{
					holder.title.setText("Requested");
				}
				holder.amount.setText(cursor.getInt(cursor.getColumnIndex(Tables.trans_amount))+"");
				holder.comment.setText(cursor.getString(cursor.getColumnIndex(Tables.trans_comment)));
				holder.time.setText(time.format(new Date(cursor.getLong(cursor.getColumnIndex(Tables.trans_timestamp)))));
				if(status==0){
					holder.time.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.transaction_status_waiting, 0);
				}
				else if(status==1){
					holder.time.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.transaction_status_success, 0);
				}
				else if(status==-1){
					holder.time.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.transaction_status_fail, 0);
				}
				
			}
			else if(view.getTag() instanceof ViewHolderLeftWithComment){
				ViewHolderLeftWithComment holder = (ViewHolderLeftWithComment) view.getTag();
				if(cursor.getInt(cursor.getColumnIndex(Tables.trans_send_req))==0){
					holder.title.setText("Sent");
				}
				else{
					holder.title.setText("Requested");
				}
				holder.amount.setText(cursor.getInt(cursor.getColumnIndex(Tables.trans_amount))+"");
				holder.comment.setText(cursor.getString(cursor.getColumnIndex(Tables.trans_comment)));
				holder.time.setText(time.format(new Date(cursor.getLong(cursor.getColumnIndex(Tables.trans_timestamp)))));
				
				
			}
			else if(view.getTag() instanceof ViewHolderRightWithoutComment){
				ViewHolderRightWithoutComment holder = (ViewHolderRightWithoutComment) view.getTag();
				if(cursor.getInt(cursor.getColumnIndex(Tables.trans_send_req))==0){
					holder.title.setText("Sent");
				}
				else{
					holder.title.setText("Requested");
				}
				holder.amount.setText(cursor.getInt(cursor.getColumnIndex(Tables.trans_amount))+"");
				holder.time.setText(time.format(new Date(cursor.getLong(cursor.getColumnIndex(Tables.trans_timestamp)))));
				if(status==0){
					holder.time.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.transaction_status_waiting, 0);
				}
				else if(status==1){
					holder.time.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.transaction_status_success, 0);
				}
				else if(status==-1){
					holder.time.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.transaction_status_fail, 0);
				}
			}
			else if(view.getTag() instanceof ViewHolderLeftWithoutComment){
				ViewHolderLeftWithoutComment holder = (ViewHolderLeftWithoutComment) view.getTag();
				if(cursor.getInt(cursor.getColumnIndex(Tables.trans_send_req))==0){
					holder.title.setText("Sent");
				}
				else{
					holder.title.setText("Requested");
				}
				holder.amount.setText(cursor.getInt(cursor.getColumnIndex(Tables.trans_amount))+"");
				holder.time.setText(time.format(new Date(cursor.getLong(cursor.getColumnIndex(Tables.trans_timestamp)))));
				
			}
//			else{
//				ViewHolderDate holder = (ViewHolderDate) view.getTag();
//				holder.wc_date.setText(date.format(new Date(cursor.getLong(cursor.getColumnIndex(Tables.trans_timestamp)))));
//			}
		}

		@Override
		public View newView(Context context, Cursor cursor, ViewGroup parent) {
//			if(cursor.getInt(cursor.getColumnIndex(Tables.trans_isDate))==1){
//				ViewHolderDate holder;
//				View view;
//				view = li.inflate(R.layout.row_chat_date, parent,false);
//				holder = new ViewHolderDate(view);
//				view.setTag(holder);
//				return view;
//			}

			if(cursor.getString(cursor.getColumnIndex(Tables.trans_comment))!=null){

				if(cursor.getInt(cursor.getColumnIndex(Tables.trans_to_from))==1){
					ViewHolderRightWithComment holder;
					View view;
					view = li.inflate(R.layout.row_chat_right_with_comment, parent, false);
					holder = new ViewHolderRightWithComment(view);
					view.setTag(holder);
					return view;
				}
				else{
					ViewHolderLeftWithComment holder;
					View view;
					view = li.inflate(R.layout.row_chat_left_with_comment, parent, false);
					holder = new ViewHolderLeftWithComment(view);
					holder.profile_pic.setImageBitmap(contact_pic);
					//holder.profile_pic.setBackgroundColor(bitmapBgColor);
					view.setTag(holder);
					return view;
				}

			}
			else{

				if(cursor.getInt(cursor.getColumnIndex(Tables.trans_to_from))==1){
					ViewHolderRightWithoutComment holder;
					View view;
					view = li.inflate(R.layout.row_chat_right_without_comment, parent, false);
					holder = new ViewHolderRightWithoutComment(view);
					view.setTag(holder);
					return view;
				}
				else{
					ViewHolderLeftWithoutComment holder;
					View view;
					view = li.inflate(R.layout.row_chat_left_without_comment, parent, false);
					holder = new ViewHolderLeftWithoutComment(view);
					holder.profile_pic.setImageBitmap(contact_pic);
					//holder.profile_pic.setBackgroundColor(bitmapBgColor);
					view.setTag(holder);
					return view;
				}

			}

		}

		
	}

	

}
