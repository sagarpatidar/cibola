package cibola.co.in.UI.Fragments;

import java.io.File;
import java.util.ArrayList;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import cibola.co.in.Adapters.NavigationDrawerListAdapter;
import cibola.co.in.Adapters.NavigationElement;
import cibola.co.in.General.CircularImageView;
import cibola.co.in.General.Common;
import cibola.co.in.General.Constants;
import cibola.co.in.General.MyAccountManager;
import cibola.co.in.Graphics.ImageBlur;
import cibola.co.in.NetworkManager.ImageType;
import cibola.co.in.UI.LoadMoney;
import cibola.co.in.UI.R;

public class NavigationDrawerFragment extends Fragment {

	private static final String STATE_SELECTED_POSITION = "selected_navigation_drawer_position";

	private NavigationDrawerCallbacks mCallbacks;
	private ActionBarDrawerToggle mDrawerToggle;
	private DrawerLayout mDrawerLayout;
	private ListView mDrawerListView;
	private View mFragmentContainerView;
	private RelativeLayout drawerProfile;
	private CircularImageView profilePic;
	private TextView amount;
	//private int blurredBackdropHeight;
	private int mCurrentSelectedPosition = 0;
	
	private MyAccountManager accountManager;
	public NavigationDrawerFragment() {
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		if (savedInstanceState != null) {
			mCurrentSelectedPosition = savedInstanceState
					.getInt(STATE_SELECTED_POSITION);
		}
		//selectItem(mCurrentSelectedPosition);
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		setHasOptionsMenu(true);
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View rootView = inflater.inflate(
				R.layout.fragment_navigation_drawer, container, false);

		drawerProfile = (RelativeLayout) rootView.findViewById(R.id.navigation_drawer_profile);
		profilePic = (CircularImageView) rootView.findViewById(R.id.profile_pic);
		
		rootView.findViewById(R.id.navigation_profile_tint).getBackground().setAlpha(Constants.PROFILE_COVER_OPACITY);
		
		TextView name = (TextView) rootView.findViewById(R.id.profile_name);
		amount = (TextView) rootView.findViewById(R.id.profile_amount);
		ImageButton add_to_wallet = (ImageButton) rootView.findViewById(R.id.profile_add_to_wallet);
		RelativeLayout rate = (RelativeLayout) rootView.findViewById(R.id.rate_us);
		
		Common.setTextViewFontStyle(Constants.FONT_ROBOTO_LIGHT, getActivity().getAssets(), amount);
		Common.setTextViewFontStyle(Constants.FONT_ROBOTO_REGULAR, getActivity().getAssets(), name);
		
		profilePic.setOnClickListener(onClickListener);
		add_to_wallet.setOnClickListener(onClickListener);
		
		rate.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Common.showToast(getActivity(), "Under construction!");
			}
		});
		
		accountManager = new MyAccountManager(getActivity());
		name.setText(accountManager.getFullname());
		amount.setText(accountManager.getBalance()+"");
	
		String PATH = 	Environment.getExternalStorageDirectory()+Constants.CIBOLA_USER_PROFILE_IMAGE_FILEPATH;
		File file = new File(PATH);
		if(file.exists()){
			final Bitmap bitmap = Bitmap.createBitmap(BitmapFactory.decodeFile(PATH));
			profilePic.setImageBitmap(bitmap);
			ImageBlur blur = new ImageBlur();
			blur.setBlurredBackground(drawerProfile, bitmap);
		}
		mDrawerListView = (ListView) rootView.findViewById(R.id.drawer_list);
		
		mDrawerListView
		.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				selectItem(position);
				//view.setBackgroundDrawable(getResources().getDrawable(R.drawable.drawer_row_selected));
			}
		});
		
		mDrawerListView.setAdapter(new NavigationDrawerListAdapter(getActionBar().getThemedContext(),
				R.layout.row_drawer, getNavigationElementList()));
		mDrawerListView.setItemChecked(mCurrentSelectedPosition, true);
		return rootView;
	}
	
	private OnClickListener onClickListener = new View.OnClickListener() {
		
		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.profile_add_to_wallet:
				getActivity().startActivity(new Intent(getActivity(), LoadMoney.class));
				break;
			case R.id.profile_pic:
				selectItem(2);
				break;
			default:
				break;
			}
			
		}
	};
	
	private ArrayList<NavigationElement> getNavigationElementList(){
		ArrayList<NavigationElement> temp = new ArrayList<NavigationElement>();
		NavigationElement row1 = new NavigationElement("Refund to Bank Account", "move money from your wallet to any bank acc.", R.drawable.drawer_icon_refund_to_bank);
		NavigationElement row2 = new NavigationElement("Edit Account Details", "update/add bank accounts", R.drawable.drawer_icon_edit_account_details);
		NavigationElement row3 = new NavigationElement("Edit Profile Details", "update username, picture and other details", R.drawable.drawer_icon_edit_profile_details);
		NavigationElement row4 = new NavigationElement("Contact Us", "Contact us for any assistance or complaints", R.drawable.drawer_icon_contact_us);
		//NavigationElement row5 = new NavigationElement("Be a Goldman", "Enjoy on the top pers and perquisets", R.drawable.drawer_icon_goldman);
		NavigationElement row6 = new NavigationElement("About RBI Privacy", "Read about our tie-up with RBI and security ", R.drawable.drawer_icon_rbi_privacy);
		NavigationElement row7 = new NavigationElement("Help", "FAQ and how to move around", R.drawable.drawer_icon_help);
		temp.add(row1);
		temp.add(row2);
		temp.add(row3);
		temp.add(row4);
		//temp.add(row5);
		temp.add(row6);
		temp.add(row7);
		return temp;
	}

	public boolean isDrawerOpen() {
		return mDrawerLayout != null
				&& mDrawerLayout.isDrawerOpen(mFragmentContainerView);
	}

	public void setUp(int fragmentId, DrawerLayout drawerLayout) {
		mFragmentContainerView = getActivity().findViewById(fragmentId);
		mFragmentContainerView.setOnClickListener(null);
		mDrawerLayout = drawerLayout;
		
		mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow,
				GravityCompat.START);
		

		ActionBar actionBar = getActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);
		actionBar.setHomeButtonEnabled(true);

		mDrawerToggle = new ActionBarDrawerToggle(getActivity(), /* host Activity */
				mDrawerLayout, /* DrawerLayout object */
				R.drawable.ic_drawer, /* nav drawer image to replace 'Up' caret */
				R.string.navigation_drawer_open, 
				R.string.navigation_drawer_close 
				) {
			@Override
			public void onDrawerClosed(View drawerView) {
				super.onDrawerClosed(drawerView);
				if (!isAdded()) {
					return;
				}

				getActivity().supportInvalidateOptionsMenu(); // calls
			}

			@Override
			public void onDrawerOpened(View drawerView) {
				super.onDrawerOpened(drawerView);
				if (!isAdded()) {
					return;
				}

				getActivity().supportInvalidateOptionsMenu(); // calls
			}
		};

		mDrawerToggle.setDrawerIndicatorEnabled(true);
		mDrawerLayout.post(new Runnable() {
			@Override
			public void run() {
				mDrawerToggle.syncState();
			}
		});

		mDrawerLayout.setDrawerListener(mDrawerToggle);
	}

	private void selectItem(int position) {
		mCurrentSelectedPosition = position;
	    
		if (mDrawerListView != null) {
			mDrawerListView.setItemChecked(position, true);
		}
		if (mDrawerLayout != null) {
			mDrawerLayout.closeDrawer(mFragmentContainerView);
		}
		if (mCallbacks != null) {
			mCallbacks.onNavigationDrawerItemSelected(position);
		}
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		try {
			mCallbacks = (NavigationDrawerCallbacks) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(
					"Activity must implement NavigationDrawerCallbacks.");
		}
	}

	@Override
	public void onDetach() {
		super.onDetach();
		mCallbacks = null;
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putInt(STATE_SELECTED_POSITION, mCurrentSelectedPosition);
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		// Forward the new configuration the drawer toggle component.
		mDrawerToggle.onConfigurationChanged(newConfig);
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

		if (mDrawerLayout != null && isDrawerOpen()) {
			inflater.inflate(R.menu.global, menu);
			showGlobalContextActionBar();
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (mDrawerToggle.onOptionsItemSelected(item)) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * Per the navigation drawer design guidelines, updates the action bar to
	 * show the global app 'context', rather than just what's in the current
	 * screen.
	 */
	private void showGlobalContextActionBar() {
		ActionBar actionBar = getActionBar();
		actionBar.setDisplayShowTitleEnabled(false);
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
		//actionBar.setTitle(R.string.app_name);
	}

	private ActionBar getActionBar() {
		
		return ((ActionBarActivity) getActivity()).getSupportActionBar();
	}
	
	public void refreshProfile(){
		if(Common.isImageExistsOnDisk(null, ImageType.Self)){
			Bitmap bitmap = Bitmap.createBitmap(BitmapFactory.decodeFile(Common.getContactImagePath(null, ImageType.Self)));
			profilePic.setImageBitmap(bitmap);
			ImageBlur blur = new ImageBlur();
			blur.setBlurredBackground(drawerProfile, bitmap);
		}
	}
	
	public void refreshBalance(){
		amount.setText(accountManager.getBalance()+"");
	}

	/**
	 * Callbacks interface that all activities using this fragment must
	 * implement.
	 */
	public static interface NavigationDrawerCallbacks {
		/**
		 * Called when an item in the navigation drawer is selected.
		 */
		void onNavigationDrawerItemSelected(int position);
	}
}
