/**
 *	DialogContactUs.java
 *  Created by Sagar Patidar on 13-Aug-2014, 5:40:24 AM
 *	Copyright � 2014 by Cibola Pvt. Ltd
 *	All rights reserved.
 */

package cibola.co.in.UI.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import cibola.co.in.General.Common;
import cibola.co.in.General.Constants;
import cibola.co.in.UI.ContactUs;
import cibola.co.in.UI.R;

public class DialogContactUs extends Fragment{

	private EditText subject;
	private EditText message;
	private TextView send;
	private TextView cancel;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.dialog_contact_us, container,
				false);
		subject = (EditText) rootView.findViewById(R.id.dialog_contact_us_subject);
		message = (EditText) rootView.findViewById(R.id.dialog_contact_us_body);
		send = (TextView) rootView.findViewById(R.id.dialog_contact_us_send);
		cancel = (TextView) rootView.findViewById(R.id.dialog_contact_us_cancel);
		Common.setTextViewFontStyle(Constants.FONT_ROBOTO_REGULAR, getActivity().getAssets(), send, cancel);
		Common.setEditTextFontStyle(Constants.FONT_ROBOTO_REGULAR, getActivity().getAssets(), subject, message);
		Common.showSoftInputKeyboard(getActivity(), subject);
		
		send.setOnClickListener(onClickListener);
		cancel.setOnClickListener(onClickListener);
		return rootView;
	}
	
	private OnClickListener onClickListener = new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.dialog_contact_us_cancel:
				dismiss();
				break;
			case R.id.dialog_contact_us_send:
				dismiss();
				break;
			default:
				break;
			}
			
		}
	};
	
	public void show(FragmentManager fragmentManager) {
	    FragmentTransaction ft = fragmentManager.beginTransaction();
	    String tag = DialogContactUs.class.getName();
	    ft.add(android.R.id.content, this, tag);
	    ft.commit();
	    ContactUs.isDialogOpen = true;
	}

	public void dismiss() {
	    FragmentTransaction ft = getFragmentManager().beginTransaction();
	    ft.remove(this);
	    ft.commit();
	    ContactUs.isDialogOpen = false;
	}
	
	
}
