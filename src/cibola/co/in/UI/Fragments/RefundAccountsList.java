/**
 *	RefundAccountsList.java
 *  Created by Sagar Patidar on Oct 16, 2014, 12:58:49 AM
 *	Copyright � 2014 by Cibola Technologies Pvt. Ltd
 *	All rights reserved.
 */

package cibola.co.in.UI.Fragments;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.CursorLoader;
import android.support.v4.widget.CursorAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;
import cibola.co.in.General.Common;
import cibola.co.in.General.Constants;
import cibola.co.in.Interfaces.RefundAccountListCallBack;
import cibola.co.in.SqliteManager.DataProvider;
import cibola.co.in.SqliteManager.Tables;
import cibola.co.in.UI.R;
import cibola.co.in.UI.RefundToBank;

public class RefundAccountsList extends ListFragment implements LoaderCallbacks<Cursor>{

	private BankAccountsAdapter adapter;
	private RefundAccountListCallBack callBack;
	private MyReceiver receiver;
	
	@Override
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		adapter = new BankAccountsAdapter(getActivity(), null, false);
		try{
			callBack = (RefundAccountListCallBack) getActivity();
		}
		catch(Exception e){
			e.printStackTrace();
		}
		
		receiver = new MyReceiver();
		IntentFilter filter = new IntentFilter(RefundToBank.GET_ACCOUNT_DETAILS_BROADCAST_ACTION);
		getActivity().registerReceiver(receiver, filter);
	}


	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		getLoaderManager().initLoader(0, null, this);

		ListView lv  = getListView();
		View emptyView = getLayoutInflater(savedInstanceState).inflate(R.layout.fragment_empty_accounts, null);
		TextView text01 = (TextView) emptyView.findViewById(R.id.text01);
		Common.setTextViewFontStyle(Constants.FONT_ROBOTO_REGULAR, getActivity().getAssets(), text01);
		ViewGroup viewGroup = (ViewGroup) lv.getParent();
		viewGroup.addView(emptyView);
		lv.setEmptyView(emptyView);
		
		View footer = getLayoutInflater(savedInstanceState).inflate(R.layout.row_bank_account_selector, null);
		TextView text1 = (TextView) footer.findViewById(R.id.text1);
		TextView text2 = (TextView) footer.findViewById(R.id.row_ba_selector_number);
		TextView add = (TextView) footer.findViewById(R.id.row_ba_selector_radio);
		text1.setText("Add and send to new account");
		text2.setText("");
		add.setText("ADD");
		Common.setTextViewFontStyle(Constants.FONT_ROBOTO_REGULAR, getActivity().getAssets(), text1, text2, add);
		add.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				callBack.onClickAddAccount();
			}
		});

		
		lv.addFooterView(footer);
		setListAdapter(adapter);
	}
	

	@Override
	public void onLoadFinished(android.support.v4.content.Loader<Cursor> arg0,
			Cursor arg1) {
		adapter.swapCursor(arg1);
	}

	@Override
	public void onLoaderReset(android.support.v4.content.Loader<Cursor> arg0) {
		adapter.swapCursor(null);
	}

	@Override
	public android.support.v4.content.Loader<Cursor> onCreateLoader(int arg0,
			Bundle arg1) {
		CursorLoader loader = new CursorLoader(getActivity(), DataProvider.URI_BANK_ACCOUNTS, null, null, null, null);
		return loader;
	}
	
	@Override
	public void onPause(){
		super.onPause();
		getActivity().unregisterReceiver(receiver);
	}
	
	@Override
	public void onResume(){	
		super.onResume();
		IntentFilter filter = new IntentFilter(RefundToBank.GET_ACCOUNT_DETAILS_BROADCAST_ACTION);
		getActivity().registerReceiver(receiver, filter);
	}


	private class BankAccountsAdapter extends CursorAdapter
	{
		private LayoutInflater li;
		private int selectPosition = -1;
		
		public BankAccountsAdapter(Context context, Cursor c, boolean autoRequery) {
			super(context, c, autoRequery);
			mContext = context;
			li = LayoutInflater.from(context);
		}

		private class ViewHolder{
			private TextView accountTitle;
			private TextView accountNumber;
			private TextView selectButton;
			public ViewHolder(View v){
				accountTitle = (TextView) v.findViewById(R.id.text1);
				accountNumber = (TextView) v.findViewById(R.id.row_ba_selector_number);
				selectButton = (TextView) v.findViewById(R.id.row_ba_selector_radio);
				Common.setTextViewFontStyle(Constants.FONT_ROBOTO_REGULAR,mContext.getAssets(), accountNumber, accountTitle, selectButton);
			}
		}
		
		public int getSelectedPosition(){
			return selectPosition;
		}

		@Override
		public View getView(final int position, final View convertView, final ViewGroup viewGroup) {
			final ViewHolder holder;
			View row = convertView;
		    if (row == null) {
		        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		        row = inflater.inflate(R.layout.row_bank_account_selector, null);
		        holder = new ViewHolder(row);
		        row.setTag(holder);
		    } else {
		        holder = (ViewHolder) convertView.getTag();
		    }
		    Cursor cursor = getCursor();
		    cursor.moveToPosition(position);
		    holder.accountNumber.setText(cursor.getString(cursor.getColumnIndex(Tables.account_number)));
		    
		    if(position==selectPosition){
		    	holder.selectButton.setText("SELECTED"); 
		    	holder.selectButton.setBackgroundResource(R.drawable.button_row_refund_account_selected);
		    	holder.selectButton.setTextColor(getResources().getColor(R.color.white));
		    }
		    else{
		    	holder.selectButton.setText("SELECT");
		    	holder.selectButton.setBackgroundResource(R.drawable.button_row_refund_account_unselected);
		    	holder.selectButton.setTextColor(getResources().getColor(R.color.refund_blue_text));
		    }
		    
		    holder.selectButton.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					selectPosition = position;
					notifyDataSetChanged();
				}
			});
		    
		    
		    return row;
		}


		@Override
		public void bindView(View view, Context context,final Cursor cursor) {
			ViewHolder holder = (ViewHolder) view.getTag();
			holder.accountNumber.setText(cursor.getString(cursor.getColumnIndex(Tables.account_number)));
			
		}

		@Override
		public View newView(Context context, Cursor cursor, ViewGroup parent) {
			ViewHolder holder;
			View view;
			view = li.inflate(R.layout.row_bank_account_selector, parent, false);
			holder = new ViewHolder(view);
			view.setTag(holder);
			return view;
		}

	}
	
	private class MyReceiver extends BroadcastReceiver{

		@Override
		public void onReceive(Context context, Intent intent) {
			int position = adapter.getSelectedPosition();
			if(position>-1){
				Cursor cursor = adapter.getCursor();
				cursor.moveToPosition(position);
				String name = cursor.getString(cursor.getColumnIndex(Tables.account_name));
				String number = cursor.getString(cursor.getColumnIndex(Tables.account_number));
				String ifsc = cursor.getString(cursor.getColumnIndex(Tables.account_ifsc));
				if(name.length()>0 && number.length()>0 && ifsc.length()>0){
					callBack.extractAccountDetails(name, number, ifsc);
				}
				else{
					Common.showToast(getActivity(), "Selected account details doesn't seems correct.");
				}
			}
			else{
				Common.showToast(getActivity(), "Please select one account.");
			}
		}

	}

}
