/**
 *	SignupHolder.java
 *  Created by Sagar Patidar on 11-Aug-2014, 1:19:49 AM
 *	Copyright � 2014 by Cibola Pvt. Ltd
 *	All rights reserved.
 */

package cibola.co.in.UI.Fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import cibola.co.in.General.Common;
import cibola.co.in.General.Constants;
import cibola.co.in.General.DialogManager;
import cibola.co.in.General.MyAccountManager;
import cibola.co.in.Graphics.ImageCrop;
import cibola.co.in.Graphics.PhotoManager;
import cibola.co.in.NetworkManager.Marketplace;
import cibola.co.in.UI.R;


public class SignupFragment extends Fragment {

	private ImageView pic;
	private TextView text01;
	private TextView text02;
	private TextView text33;
	private EditText date;
	private TextView done;
	private RadioGroup radioGroup;
	private View rootView;
	private PhotoManager photoManager;
	public int PICK_FROM_FILE_CROP = 1;
	
	protected Context mContext;
	public SignupFragment(Context c) {
		mContext = c;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		rootView = inflater.inflate(R.layout.fragment_sign_up,
				container, false);

		pic = (ImageView) rootView.findViewById(R.id.add_profile_pic);
		text01 = (TextView) rootView.findViewById(R.id.text01);
		text02 = (TextView) rootView.findViewById(R.id.text02);
		text33 = (TextView) rootView.findViewById(R.id.text33);
		done = (TextView) rootView.findViewById(R.id.signup_done);
		date = (EditText) rootView.findViewById(R.id.date);
		radioGroup = (RadioGroup) rootView.findViewById(R.id.signup_gender);
		Common.setEditTextFontStyle(Constants.FONT_ROBOTO_REGULAR,mContext.getAssets(), date);
		Common.setTextViewFontStyle(Constants.FONT_ROBOTO_REGULAR, mContext.getAssets(), text02,text01,text33,done);
		
		photoManager = new PhotoManager();

		pic.setOnClickListener(onClickListener);
		date.setOnClickListener(onClickListener);
		done.setOnClickListener(onClickListener);
		/*----------------------Functionality--------------------------*/
		return rootView;
	}

	private void selectProfileImage(){
		Log.d("DEBUG#1", "CALLED");
		ImageCrop crop = new ImageCrop();
		Intent cropImageIntent = crop.cropImage();
		((Activity) mContext).startActivityForResult(cropImageIntent,
				PICK_FROM_FILE_CROP);
	}

	public void setProfileImage(){
		Bitmap bitmap = photoManager.getSelfImage();
		if(bitmap!=null){
			pic.setImageBitmap(bitmap);	
		}
	}
	
	private OnClickListener onClickListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			switch(v.getId()){
			case R.id.add_profile_pic:	
				selectProfileImage();
				break;
			case R.id.date:
				DialogManager.showDateDialog(mContext,date);
				break;
			case R.id.signup_done:
				if(date.getText().toString().length()<1){
					Common.showToast(mContext, "Please input your Date of birth.");
				}
				else{
					Marketplace marketplace = new Marketplace(getActivity());
					MyAccountManager accountManager = new MyAccountManager(getActivity());
					String _mobile = accountManager.getMobileNumber();
					if(_mobile!=null){
						marketplace.signupStepThree(getActivity(),_mobile, ((RadioButton) rootView.findViewById(
							radioGroup.getCheckedRadioButtonId())).getText().toString() , date.getText().toString(),
							Environment.getExternalStorageDirectory()+Constants.CIBOLA_USER_PROFILE_IMAGE_FILEPATH);
					}
					else{
						if(Constants.DEBUG_MODE)
							Log.d(Constants.BUG, "Uh Oh! Where is the mobile number?");
					}
				}
				break;
			default:
				Log.d("DEBUG","Clicked button not found");
				break;	
			}
		}
	};

	
}

