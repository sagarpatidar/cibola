package cibola.co.in.UI.Fragments;

import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Locale;
import java.util.Set;
import java.util.TimeZone;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.CursorLoader;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.CursorAdapter;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.SearchView.OnCloseListener;
import android.support.v7.widget.SearchView.OnQueryTextListener;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import cibola.co.in.General.Common;
import cibola.co.in.General.Constants;
import cibola.co.in.Graphics.PhotoManager;
import cibola.co.in.NetworkManager.ImageType;
import cibola.co.in.SqliteManager.DataProvider;
import cibola.co.in.SqliteManager.Tables;
import cibola.co.in.UI.ChatActivity;
import cibola.co.in.UI.MainActivity;
import cibola.co.in.UI.R;

public class TransactionFragment extends ListFragment implements OnQueryTextListener, OnCloseListener,
LoaderCallbacks<Cursor> {


	private TransactionsAdapter adapter;

	private MySearchView mSearchView;
	private String mCurFilter;
	private String[] projections = new String[]{Tables.contact_serial,
			Tables.contact_mobile, Tables.contact_name,Tables.contact_amount, Tables.contact_send_req, 
			Tables.contact_to_from, Tables.contact_comment, Tables.contact_unread_transaction_count, 
			Tables.contact_onCibola,Tables.contact_background};

	@Override
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		adapter = new TransactionsAdapter(getActivity(), null, false);
	}	

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		setHasOptionsMenu(true);
		setListShown(false);

		getLoaderManager().initLoader(0, null, this);
		ListView lv  = getListView();
		lv.setDividerHeight(1);

		View emptyView = getLayoutInflater(savedInstanceState).inflate(R.layout.fragment_empty_history_view, null);
		TextView text01 = (TextView) emptyView.findViewById(R.id.text01);
		TextView text02 = (TextView) emptyView.findViewById(R.id.text02);
		TextView text03 = (TextView) emptyView.findViewById(R.id.text03);
		Common.setTextViewFontStyle(Constants.FONT_ROBOTO_REGULAR, getActivity().getAssets(), text01, text02, text03);

		ViewGroup viewGroup = (ViewGroup) lv.getParent();
		viewGroup.addView(emptyView);
		lv.setEmptyView(emptyView);

		/*		lv.setMultiChoiceModeListener(new MultiChoiceModeListener() {

			private int selectionCount = 0;

			@Override
			public boolean onPrepareActionMode(ActionMode actionMode, Menu menu) {
				return false;
			}

			@Override
			public void onDestroyActionMode(ActionMode actionMode) {
				adapter.clearSelection();
			}

			@Override
			public boolean onCreateActionMode(ActionMode actionMode, Menu menu) {
				MenuInflater inflater = getActivity().getMenuInflater();
				inflater.inflate(R.menu.contextual_menu, menu);
				return true;
			}

			@Override
			public boolean onActionItemClicked(ActionMode actionMode, MenuItem item) {
				switch (item.getItemId()) {
				case R.id.item_delete:
					selectionCount = 0;
					adapter.clearSelection();
					actionMode.finish();
				}
				return false;
			}

			@Override
			public void onItemCheckedStateChanged(ActionMode actionMode, int position, long id,
					boolean checked) {
				if (checked) {
					selectionCount++;
					adapter.setNewSelection(position, checked);                  
				} else {
					selectionCount--;
					adapter.removeSelection(position);               
				}
				actionMode.setTitle(selectionCount + " selected");

			}
		});
		lv.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);

		lv.setOnItemLongClickListener(new OnItemLongClickListener() {

			@Override
			public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
					int position, long arg3) {
				// TODO Auto-generated method stub

				getListView().setItemChecked(position, !adapter.isPositionChecked(position));
				return false;
			}
		});*/


		setListAdapter(adapter);
	}





	@Override
	public void onListItemClick(ListView l, View v, int position, long id){
		super.onListItemClick(l, v, position, id);
		Intent intent = new Intent(getActivity(), ChatActivity.class);
		intent.putExtra(Constants.CONTACT_NAME, ((TextView)v.findViewById(R.id.row_transaction_name)).getText().toString());
		intent.putExtra(Constants.CONTACT_ID, ((TextView)v.findViewById(R.id.row_transaction_id)).getText().toString());
		Cursor cursor = adapter.getCursor();
		cursor.moveToPosition(position);
		intent.putExtra(Constants.CONTACT_BACKGROUND, cursor.getInt(cursor.getColumnIndex(Tables.contact_background)));
		startActivity(intent);
	}

	@Override
	public void onResume(){
		super.onResume();
		getLoaderManager().restartLoader(0, null, this);
		Log.d("Transaction Fragment","Resumed");
	}

	public static class MySearchView extends SearchView {
		public MySearchView(Context context) {
			super(context);
		}

		@Override
		public void onActionViewCollapsed() {
			setQuery("", false);
			super.onActionViewCollapsed();
		}
	}



	@Override 
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		if(!MainActivity.mNavigationDrawerFragment.isDrawerOpen()){
			MenuItem item =  menu.findItem(R.id.action_search);
			
			MenuItemCompat.setShowAsAction( item, MenuItemCompat.SHOW_AS_ACTION_IF_ROOM |
					MenuItemCompat.SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW);
			item.setIcon(R.drawable.icon_search);
			mSearchView = new MySearchView(getActivity());
			mSearchView.setOnQueryTextListener(this);
			mSearchView.setOnCloseListener(this);
			mSearchView.setQueryHint("Search Recent Transactions");
			
			MenuItemCompat.setActionView(item, mSearchView);
			customizeSearchView(mSearchView);
		}
	}
	
	private void customizeSearchView(MySearchView mSearchView){
		EditText v = (EditText) mSearchView.findViewById(R.id.search_src_text);
		v.setTextColor(getResources().getColor(R.color.white));
		v.setHintTextColor(getResources().getColor(R.color.white));
	}

	@Override
	public boolean onQueryTextChange(String newText) {
		String newFilter = !TextUtils.isEmpty(newText) ? newText : null;
		if (mCurFilter == null && newFilter == null) {
			return true;
		}
		if (mCurFilter != null && mCurFilter.equals(newFilter)) {
			return true;
		}
		mCurFilter = newFilter;
		getLoaderManager().restartLoader(0, null, this);
		return true;
	}

	@Override 
	public boolean onQueryTextSubmit(String query) {
		// Don't care about this.
		return true;
	}

	@Override
	public boolean onClose() {
		if (!TextUtils.isEmpty(mSearchView.getQuery())) {
			mSearchView.setQuery(null, true);
		}
		return true;
	}

	@Override
	public void onLoadFinished(android.support.v4.content.Loader<Cursor> arg0,
			Cursor arg1) {
		adapter.swapCursor(arg1);
		if (isResumed()) {
			setListShown(true);
		} else {
			setListShownNoAnimation(true);
		}

	}

	@Override
	public void onLoaderReset(android.support.v4.content.Loader<Cursor> arg0) {
		adapter.swapCursor(null);
	}



	@Override
	public android.support.v4.content.Loader<Cursor> onCreateLoader(int arg0,
			Bundle arg1) {
		CursorLoader loader ;
		if(mCurFilter==null || mCurFilter.length()==0){
			loader = new CursorLoader(getActivity(), DataProvider.URI_CONTACTS, projections, 
					Tables.contact_total_transaction_count+">0", null, Tables.contact_timestamp+" DESC");
		}
		else{
			String where = Tables.contact_total_transaction_count+">0 AND ("+Tables.contact_name+" LIKE '"+mCurFilter+"%' OR "+Tables.contact_name+" LIKE '% "+mCurFilter+"%')";
			loader = new CursorLoader(getActivity(), DataProvider.URI_CONTACTS, projections, 
					where, null, Tables.contact_timestamp+" DESC"); 
		}

		return loader;
	}

	private class TransactionsAdapter extends CursorAdapter
	{
		private Context mContext;
		private LayoutInflater li;
		private SimpleDateFormat date;
		private PhotoManager photoManager;
		private HashMap<Integer, Boolean> mSelection = new HashMap<Integer, Boolean>();

		public TransactionsAdapter(Context context, Cursor c, boolean autoRequery) {
			super(context, c, autoRequery);
			mContext = context;
			li = LayoutInflater.from(context);
			date = new SimpleDateFormat("MMM dd, yyyy",Locale.US);
			date.setTimeZone(TimeZone.getTimeZone("IST"));
			photoManager = new PhotoManager();
		}

		public void setNewSelection(int position, boolean value) {
			mSelection.put(position, value);

			notifyDataSetChanged();
		}

		public boolean isPositionChecked(int position) {
			Boolean result = mSelection.get(position);
			return result == null ? false : result;
		}

		public Set<Integer> getCurrentCheckedPosition() {
			return mSelection.keySet();
		}

		public void removeSelection(int position) {
			mSelection.remove(position);
			notifyDataSetChanged();
		}

		public void clearSelection() {
			mSelection = new HashMap<Integer, Boolean>();
			notifyDataSetChanged();
		}

		private class ViewHolder_Read{
			private TextView id;
			private TextView name;
			private TextView amount_title;
			private TextView amount;
			private TextView comment;

			private ImageView profile_pic;
			public ViewHolder_Read(View v){
				id = (TextView) v.findViewById(R.id.row_transaction_id);
				name = (TextView) v.findViewById(R.id.row_transaction_name);
				amount_title = (TextView) v.findViewById(R.id.row_transaction_title);
				amount = (TextView) v.findViewById(R.id.row_transaction_amount);
				comment = (TextView) v.findViewById(R.id.row_transaction_comment);
				profile_pic = (ImageView) v.findViewById(R.id.row_transaction_contact_pic);
				Common.setTextViewFontStyle(Constants.FONT_ROBOTO_REGULAR,getActivity().getAssets(),name,comment, amount, amount_title);
			}
		}

		private class ViewHolder_Unread{
			private TextView id;
			private TextView name;
			private TextView amount_title;
			private TextView amount;
			private TextView comment;
			private TextView unreadCount;
			private ImageView profile_pic;


			public ViewHolder_Unread(View v){
				id = (TextView) v.findViewById(R.id.row_transaction_id);
				name = (TextView) v.findViewById(R.id.row_transaction_name);
				amount_title = (TextView) v.findViewById(R.id.row_transaction_title);
				amount = (TextView) v.findViewById(R.id.row_transaction_amount);
				comment = (TextView) v.findViewById(R.id.row_transaction_comment);
				unreadCount = (TextView) v.findViewById(R.id.row_transaction_unread_count);
				profile_pic = (ImageView) v.findViewById(R.id.row_transaction_contact_pic);
				Common.setTextViewFontStyle(Constants.FONT_ROBOTO_REGULAR,getActivity().getAssets(),amount_title,comment);
				Common.setTextViewFontStyle(Constants.FONT_ROBOTO_REGULAR,getActivity().getAssets(),unreadCount,name,amount);
			}
		}

		@Override
		public int getItemViewType(int position) {
			Cursor cursor = (Cursor) getItem(position);
			int unread = cursor.getInt(cursor.getColumnIndex(Tables.contact_unread_transaction_count));
			if(unread == 0){
				return 0;
			}
			else{
				return 1;
			}
		}

		@Override
		public int getViewTypeCount() {
			return 2;
		}

		@Override
		public void bindView(View view, Context context, Cursor cursor) {
			if(view.getTag() instanceof ViewHolder_Read){
				ViewHolder_Read holder = (ViewHolder_Read) view.getTag();
				String _name = cursor.getString(cursor.getColumnIndex(Tables.contact_name));
				String _number = cursor.getLong(cursor.getColumnIndex(Tables.contact_mobile))+"";
				holder.id.setText(_number);
				holder.name.setText(Html.fromHtml(Common.getSearchHighlightedString(mCurFilter, _name)), 
						TextView.BufferType.SPANNABLE);
				if(cursor.getInt(cursor.getColumnIndex(Tables.contact_to_from))==0){
					if(cursor.getInt(cursor.getColumnIndex(Tables.contact_send_req))==0){
						holder.amount_title.setText("Sent");
					}
					else{
						holder.amount_title.setText("Requested");
					}
				}
				else{
					if(cursor.getInt(cursor.getColumnIndex(Tables.contact_send_req))==0){
						holder.amount_title.setText("You : Sent");
					}
					else{
						holder.amount_title.setText("You : Requested");
					}	
				}
				holder.amount.setText(cursor.getInt(cursor.getColumnIndex(Tables.contact_amount))+"");
				String comment = cursor.getString(cursor.getColumnIndex(Tables.contact_comment));
				if(comment==null){
					holder.comment.setText("");
				}
				else{
					holder.comment.setText(" | "+comment);
				}

				holder.profile_pic.setBackgroundColor(cursor.getInt(cursor.getColumnIndex(Tables.contact_background)));
				holder.profile_pic.setImageBitmap(photoManager.getCibolaUserPic(mContext, _number, _name, ImageType.Medium));

			}
			else{
				ViewHolder_Unread holder = (ViewHolder_Unread) view.getTag();
				final String _name = cursor.getString(cursor.getColumnIndex(Tables.contact_name));
				final String _number = cursor.getLong(cursor.getColumnIndex(Tables.contact_mobile))+""; 
				holder.id.setText(_number);

				holder.name.setText(Html.fromHtml(Common.getSearchHighlightedString(mCurFilter, _name)), 
						TextView.BufferType.SPANNABLE);
				if(cursor.getInt(cursor.getColumnIndex(Tables.contact_send_req))==0){
					holder.amount_title.setText("Sent");
				}
				else{
					holder.amount_title.setText("Requested");
				}
				holder.amount.setText(cursor.getInt(cursor.getColumnIndex(Tables.contact_amount))+"");
				String comment = cursor.getString(cursor.getColumnIndex(Tables.contact_comment));
				if(comment==null){
					holder.comment.setText("");
				}
				else{
					holder.comment.setText(" | "+comment);
				}
				holder.unreadCount.setText(""+cursor.getInt(
						cursor.getColumnIndex(Tables.contact_unread_transaction_count)));

				holder.profile_pic.setBackgroundColor(cursor.getInt(cursor.getColumnIndex(Tables.contact_background)));
				holder.profile_pic.setImageBitmap(photoManager.getCibolaUserPic(mContext, _number, _name, ImageType.Medium));

			}
		}

		@Override
		public View newView(Context context, Cursor cursor, ViewGroup parent) {

			if(cursor.getInt(cursor.getColumnIndex(Tables.contact_unread_transaction_count))==0){
				ViewHolder_Read holder;
				View view;
				view = li.inflate(R.layout.row_transaction_read, parent, false);
				holder = new ViewHolder_Read(view);
				view.setTag(holder);
				return view;	
			}
			else{
				ViewHolder_Unread holder;
				View view;
				view = li.inflate(R.layout.row_transaction_unread, parent, false);
				holder = new ViewHolder_Unread(view);
				view.setTag(holder);
				return view;
			}
		}
	}
}
