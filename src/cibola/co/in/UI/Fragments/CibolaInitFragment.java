/**
 *	CibolaInitFragment.java
 *  Created by Sagar Patidar on 29-Aug-2014, 2:29:18 PM
 *	Copyright � 2014 by Cibola Technologies Pvt. Ltd
 *	All rights reserved.
 */

package cibola.co.in.UI.Fragments;

import org.json.JSONObject;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import cibola.co.in.General.Common;
import cibola.co.in.General.Constants;
import cibola.co.in.Interfaces.WebConnectionCallbacks;
import cibola.co.in.NetworkManager.NetworkConnectionDetector;
import cibola.co.in.NetworkManager.RequestType;
import cibola.co.in.NetworkManager.Services.ContactsService;
import cibola.co.in.UI.MainActivity;
import cibola.co.in.UI.R;

public class CibolaInitFragment extends Fragment implements WebConnectionCallbacks{
	
	
	public CibolaInitFragment(){
	}
	
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_cibola_init,
				container, false);
	
		//ImageView logo =(ImageView) rootView.findViewById(R.id.init_logo);
		TextView text = (TextView) rootView.findViewById(R.id.init_text01);
		Common.setTextViewFontStyle(Constants.FONT_ROBOTO_REGULAR, getActivity().getAssets(), text);
		if(NetworkConnectionDetector.isConnected(getActivity())){
			Intent intent = new Intent(getActivity(),ContactsService.class);
			intent.putExtra(Constants.CONTACTS_MANAGER_IS_INIT, 1);
			getActivity().startService(intent);	
		}
		
		return rootView;
	}

	@Override
	public void onRequestComplete(RequestType requestType, String response) {
		if(response==null){
			Log.d(Constants.BUG, "LoginActivity:onRequestComplete getting null response");
			return;
		}
		try{
			JSONObject jobj = new JSONObject(response);
			switch (requestType) {
			case RequestTypeInitContacts:
				if(jobj.getInt(Constants.WEB_RESPONSE_SUCCESS)==1){
					Log.d("WEB_SUCCESS","successfully initialized:-"+response);
					startActivity(new Intent(getActivity(),MainActivity.class));
				}
				else{
					Log.d("Login Init ERROR", jobj.getString(Constants.WEB_RESPONSE_MESSAGE));
				}
				break;
			default:
				break;
			}
		}
		catch(Exception e){
			e.printStackTrace();
		}
		
	}

	@Override
	public void onRequestError(RequestType requestType, String errorResponse) {
		// TODO Auto-generated method stub
		
	}
	
}
