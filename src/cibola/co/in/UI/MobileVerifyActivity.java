package cibola.co.in.UI;

import org.json.JSONObject;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.TransitionDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ViewFlipper;
import cibola.co.in.General.Common;
import cibola.co.in.General.Constants;
import cibola.co.in.General.MyAccountManager;
import cibola.co.in.Interfaces.WebConnectionCallbacks;
import cibola.co.in.NetworkManager.Marketplace;
import cibola.co.in.NetworkManager.RequestType;

public class MobileVerifyActivity extends ActionBarActivity implements WebConnectionCallbacks{

	private ImageView logo;
	private EditText inputCode;
	private MyReceiver receiver;
	private MyAccountManager accountManager;
	private ViewFlipper flipper;
	private Handler handler;
	private int slideCount = 1;
	private static final int OTP_LENGTH = 6;
	private boolean isVerifying = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_mobile_verify);
		getSupportActionBar().hide();
		logo = (ImageView) findViewById(R.id.cibola_logo);
		TextView title = (TextView) findViewById(R.id.mobile_verify_title);
		TextView subTitle = (TextView) findViewById(R.id.mobile_verify_subtitle);
		inputCode = (EditText) findViewById(R.id.mobile_verify_input);
		flipper = (ViewFlipper) findViewById(R.id.verify_view_flipper);
		ImageView submit = (ImageView) findViewById(R.id.mobile_verify_input_submit);

		Common.setTextViewFontStyle(Constants.FONT_ROBOTO_REGULAR,getAssets(), title, subTitle, inputCode);
		
		accountManager = new MyAccountManager(this);
		title.setText("A code has been sent to you on "+accountManager.getMobileNumber()+" via sms.");
		int startColor = Color.rgb(255, 102, 153);
		int endColor = Color.rgb(153, 102, 255);
		ColorDrawable[] color = { new ColorDrawable(startColor),
				new ColorDrawable(endColor)};
		TransitionDrawable trans = new TransitionDrawable(color);
		logo.setBackgroundDrawable(trans);
		trans.startTransition(6000);

		submit.setOnClickListener(onClickListener);

		this.receiver = new MyReceiver();
		IntentFilter filter = new IntentFilter(Constants.OTP_ACTION);
		registerReceiver(receiver, filter);
		
		handler = new Handler();
		handler.post(mUpdateTimeTask);
		
		
		if(accountManager.isRecievedVerificationCode() && !isVerifying){
			isVerifying = true;
			inputCode.setText(accountManager.getVerificationCode()+"");
			submit.performClick();
		}
	}
	
	private Runnable mUpdateTimeTask = new Runnable() {
	       public void run() {
	    	   if(slideCount<=6){
					flipper.showNext();
					slideCount++;
				}
	    	   handler.postDelayed(this, 10000);
	       }
       };
	

	private OnClickListener onClickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.mobile_verify_input_submit:
				if(inputCode.getText().toString().length()==OTP_LENGTH){
					onCodeReceive(Integer.parseInt(inputCode.getText().toString()));
				}
				else{
					Common.showToast(MobileVerifyActivity.this, "Please input a valid code");
				}
				break;

			default:
				break;
			}

		}
	};

	private void onCodeReceive(int code){
		Marketplace marketplace = new Marketplace(MobileVerifyActivity.this);
		MyAccountManager accountManager = new MyAccountManager(this);
		String _mobile = accountManager.getMobileNumber();
		if(_mobile!=null){
			marketplace.signupStepTwo(MobileVerifyActivity.this,_mobile, code);
		}
		else{
			if(Constants.DEBUG_MODE)
				Log.d(Constants.BUG, "Uh Oh! Where is the mobile number?");
		}
	}

	@Override
	public void onRequestComplete(RequestType requestType, String response){
		if(response==null){
			Common.showToast(getApplicationContext(), "Oops! Some error Occurred.");
			return;
		}
		switch (requestType) {
		case RequestTypeSignUpStepTwo:
			Log.d("RequestTypeMobileVerifyStepTwo",response);
			JSONObject json;
			try {
				json = new JSONObject(response);
				if(json.getInt(Constants.WEB_RESPONSE_SUCCESS)==1){
					Intent i = new Intent(MobileVerifyActivity.this, MainActivity.class);
					i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
					startActivity(i);
					finish();
				}
				else{
					Common.showToast(getApplicationContext(), "Oops! Code Mismatch. Please try again.");
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			break;
		default:
			break;
		}

	}
	
	public void onVerificationSMSReceived(String sms){
		if(!isVerifying){
			isVerifying = true;
			Log.d("MobileVerifyActivity Message Received",sms);
			String[] elems = sms.split(" ");
			String _code = elems[elems.length-1];
			try{
				onCodeReceive(Integer.parseInt(_code));
			}
			catch(Exception e){
				e.printStackTrace();
			}
		}
	}

	@Override
	public void onRequestError(RequestType requestType, String errorResponse) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onPause(){
		super.onPause();
		unregisterReceiver(receiver);
	}
	
	@Override
	public void onResume(){	
		super.onResume();
		IntentFilter filter = new IntentFilter(Constants.OTP_ACTION);
		registerReceiver(receiver, filter);
	}
	
	
	private class MyReceiver extends BroadcastReceiver{

		@Override
		public void onReceive(Context context, Intent intent) {
			String verificationMsg = intent.getStringExtra(Constants.OTP_BROADCAST_MESSAGE);
			if(verificationMsg!=null)
				onVerificationSMSReceived(verificationMsg);
			else
				Log.d("#DEBUG","RECEIVED NULL MSG");

		}

	}

}
