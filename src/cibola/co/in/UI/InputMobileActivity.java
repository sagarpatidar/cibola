package cibola.co.in.UI;

import java.util.regex.Pattern;

import org.json.JSONException;
import org.json.JSONObject;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v7.app.ActionBarActivity;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.util.Patterns;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import cibola.co.in.General.Common;
import cibola.co.in.General.Constants;
import cibola.co.in.General.MyAccountManager;
import cibola.co.in.Interfaces.WebConnectionCallbacks;
import cibola.co.in.NetworkManager.Marketplace;
import cibola.co.in.NetworkManager.RequestType;

public class InputMobileActivity extends ActionBarActivity implements WebConnectionCallbacks{

	private TextView title;
	private TextView button_continue;
	private EditText country_code;
	private EditText mobile;
	private EditText fullname;
	private EditText emailID;

	private String id = null;
	private String email = null;
	private String phone = null;
	private String accountName = null;
	private String name = null;

	private Pattern fullNamePattern = Pattern.compile("[a-zA-Z]+ [a-zA-Z]+");
	private Pattern emailPattern = Patterns.EMAIL_ADDRESS;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_input_mobile);
		getSupportActionBar().hide();

		title = (TextView) findViewById(R.id.input_mobile_title);
		button_continue = (TextView) findViewById(R.id.input_mobile_continue);
		country_code = (EditText) findViewById(R.id.input_mobile_country_code);
		mobile = (EditText) findViewById(R.id.input_mobile);
		fullname = (EditText) findViewById(R.id.input_name);
		emailID = (EditText) findViewById(R.id.input_email);

		Common.setEditTextFontStyle(Constants.FONT_ROBOTO_REGULAR,getAssets(), country_code, mobile);
		Common.setTextViewFontStyle(Constants.FONT_ROBOTO_REGULAR,getAssets(), title,button_continue);
		
		button_continue.setOnClickListener(onClickListener);
		mobile.setOnKeyListener(keyListener);
		getNameEmail();
	}

	private OnKeyListener keyListener = new OnKeyListener() {
		
		@Override
		public boolean onKey(View v, int keyCode, KeyEvent event) {
			if ((event.getAction() == KeyEvent.ACTION_UP) &&
					(keyCode == KeyEvent.KEYCODE_ENTER)){
				switch (v.getId()) {
				case R.id.input_mobile:
					Common.hideKeyBoardFromEditText(mobile, InputMobileActivity.this);
					button_continue.performClick();
					return true;
				default:
					break;
				}
			}
			return false;
		}
	};

	
	private OnClickListener onClickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			switch(v.getId()){
			case R.id.input_mobile_continue:
				String mob = mobile.getText().toString();
				if(mob.length()!=10){
					showToast("Please input a valid mobile number");
				}
				else{
					String _fullname = fullname.getText().toString().trim();
					
					if(fullNamePattern.matcher(_fullname).matches()){
						if(emailPattern.matcher(emailID.getText().toString()).matches()){
							
							String _fname = _fullname.split(" ")[0];
							String _lname = _fullname.split(" ")[1];
							String _email = emailID.getText().toString();
							String _mobile = mobile.getText().toString();
							MyAccountManager account = new MyAccountManager(getApplicationContext());
							boolean result = account.addParameters(new String[]{MyAccountManager.SHARED_PREFERENCES_VARIABLE_FIRSTNAME,
									MyAccountManager.SHARED_PREFERENCES_VARIABLE_LASTNAME, MyAccountManager.SHARED_PREFERENCES_VARIABLE_EMAIL,
									MyAccountManager.SHARED_PREFERENCES_VARIABLE_MOBILE}, 
									new String[]{_fname,_lname,_email,_mobile});
							Log.d("IF MOBILE SET",result+"");
							Marketplace marketplace = new Marketplace(InputMobileActivity.this);
							marketplace.signupStepOne(InputMobileActivity.this,_fname, _lname, _email, _mobile);
							// start WebRequest here.
							
						}
						else{
							showToast("Please input a valid email.");
						}
					}
					else{
						showToast("Please input your firstname and lastname");
					}

				}
				break;
			default:
				break;
			}

		}

	};


	private void getNameEmail(){
		TelephonyManager info = (TelephonyManager)getApplicationContext().getSystemService(TELEPHONY_SERVICE);
		String phoneNumber = info.getLine1Number();
		Log.d("Account Details", phoneNumber+"");

		final AccountManager manager = AccountManager.get(this);
		final Account[] accounts = manager.getAccountsByType("com.google");
		Log.d("DEBUG",accounts[0].name);
		if (accounts[0].name != null) {
			accountName = accounts[0].name;

			ContentResolver cr = this.getContentResolver();
			Cursor emailCur = cr.query(
					ContactsContract.CommonDataKinds.Email.CONTENT_URI, null,
					ContactsContract.CommonDataKinds.Email.DATA + " = ?",
					new String[] { accountName }, null);
			
			while (emailCur.moveToNext()) {
				id = emailCur.getString(emailCur.getColumnIndex(ContactsContract.CommonDataKinds.Email.CONTACT_ID));
				email = emailCur.getString(emailCur.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA));
				String newName = emailCur.getString(emailCur.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
				Log.v(Constants.DEBUG,newName+"##"+name+"##"+email);
				if (name == null || newName.length() > name.length())
					name = newName;
				validateInputs(name, email);
			}

			emailCur.close();
			if (id != null) {
				// get the phone number
				Cursor pCur = cr.query(
						ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
						null, ContactsContract.CommonDataKinds.Phone.CONTACT_ID
						+ " = ?", new String[] { id }, null);
				while (pCur.moveToNext()) {
					phone = pCur
							.getString(pCur
									.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
					Log.v("Got contacts #2", "phone" + phone);
				}
				pCur.close();
			}
		}	
	}

	private void validateInputs(String inputName, String inputEmail){
		if(fullNamePattern.matcher(inputName).matches()){
			fullname.setText(inputName);
			if(emailPattern.matcher(inputEmail).matches()){
				emailID.setText(inputEmail);

				mobile.requestFocus();
				Common.showSoftInputKeyboard(this, mobile);
			}
			else{
				if(inputEmail.length()>0){
					showToast("Please input a valid email id");
					emailID.requestFocus();
					Common.showSoftInputKeyboard(this, emailID);
				}
			}
		}
		else{
			if(inputName.length()>0){
				showToast("Please input full Name");
				fullname.requestFocus();
				Common.showSoftInputKeyboard(this, fullname);
			}
			
			if(emailPattern.matcher(inputEmail).matches()){
				emailID.setText(inputEmail);

				fullname.requestFocus();
				Common.showSoftInputKeyboard(this, mobile);
			}
			else{
				if(inputEmail.length()>0){
					showToast("Please input a valid email id");
					emailID.requestFocus();
					Common.showSoftInputKeyboard(this, emailID);
				}
			}
		}
	}

	private void showToast(String text){
		Toast.makeText(this, text, Toast.LENGTH_LONG).show();
	}


	@Override
	public void onRequestComplete(RequestType requestType, String response) {
		if(response==null){
			Common.showToast(getApplicationContext(), "Oops! Some error Occurred");
			return;
		}
		switch (requestType) {
		case RequestTypeSignUpStepOne:
			Log.d("RequestTypeMobileVerifyStepOne",response);
			try {
				JSONObject json = new JSONObject(response);
				if(json.getInt(Constants.WEB_RESPONSE_SUCCESS)==1){
					Intent i = new Intent(InputMobileActivity.this, SignUpActivity.class);
					i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
					startActivity(i);
					finish();
				}
				else{
					Common.showToast(getApplicationContext(), "Oops! Either user already exists or some error occurred.");
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
			break;
		default:
			break;
		}
	}

	@Override
	public void onRequestError(RequestType requestType, String errorResponse) {

	}

}
