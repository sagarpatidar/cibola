/**
 *	ManageAccounts.java
 *  Created by Sagar Patidar on 12-Aug-2014, 11:00:57 PM
 *	Copyright � 2014 by Cibola Pvt. Ltd
 *	All rights reserved.
 */

package cibola.co.in.UI;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import cibola.co.in.General.Common;
import cibola.co.in.General.Constants;
import cibola.co.in.Interfaces.EditAccountCallbacks;
import cibola.co.in.SqliteManager.Database;

public class ManageAccounts extends ActionBarActivity implements EditAccountCallbacks{

	private ImageView buttonOkay;
	private EditText accountName;
	private EditText accountNumber;
	private EditText accountIFSC;
	private Database db;
	private String oldAccountNumber = "";
	private boolean editFunctionalityEnabled = false; // false=> add true=> update
	
	@Override
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
	    setContentView(R.layout.fragment_edit_account_details);
		
		db = new Database(this);
		TextView text1 = (TextView) findViewById(R.id.manage_acc_text1);
		TextView text2 = (TextView) findViewById(R.id.manage_acc_text2);
		TextView text3 = (TextView) findViewById(R.id.manage_acc_text3);
		TextView text4 = (TextView) findViewById(R.id.manage_acc_text4);

		accountName = (EditText) findViewById(R.id.manage_account_account_name);
		accountNumber = (EditText) findViewById(R.id.manage_account_account_number);
		accountIFSC = (EditText) findViewById(R.id.manage_account_bank_ifsc);

		buttonOkay = (ImageView) findViewById(R.id.manage_account_button_add_okay);
		Common.setTextViewFontStyle(Constants.FONT_ROBOTO_REGULAR,getAssets(), text1, text2, text3, text4);
		Common.setEditTextFontStyle(Constants.FONT_ROBOTO_REGULAR,getAssets(), accountName, accountNumber, accountIFSC);
		Common.setupActionBar(getSupportActionBar());
		buttonOkay.setOnClickListener(onClickListener);
		
	}

	
	private void resetInputFields(){
		accountName.clearComposingText();
		accountNumber.clearComposingText();
		accountIFSC.clearComposingText();
		accountName.setText("");
		accountNumber.setText("");
		accountIFSC.setText("");
	}

	private OnClickListener onClickListener = new View.OnClickListener() {

		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.manage_account_button_add_okay:
				// add account
				if(!editFunctionalityEnabled){
					String name = accountName.getText().toString();
					String accNo = accountNumber.getText().toString();
					String ifsc = accountIFSC.getText().toString();
					if(name.length()>0 && ifsc.length()>0 && 
							accNo.length()>0){
						boolean result = db.addAccount(accNo, name, ifsc);
						if(result){
							resetInputFields();
						}
						else{
							Common.showToast(getApplicationContext(), "Account already exists or some error occured.");
						}
						
					}
					else{
						Common.showToast(getApplicationContext(), "Please input valid details.");
					}
				}
				else{
					String name = accountName.getText().toString();
					String accNo = accountNumber.getText().toString();
					String ifsc = accountIFSC.getText().toString();
					if(name.length()>0 && ifsc.length()>0 && 
							accNo.length()>0){
						//Log.d(Constants.DEBUG,	 accNo+" "+oldAccountNumber);
						if(accNo.equals(oldAccountNumber)){
							db.updateAccount(accNo, name, ifsc);
							
						}
						else{
							db.removeAccount(oldAccountNumber);
							db.addAccount(accNo, name, ifsc);
						}
						oldAccountNumber = "";
						editFunctionalityEnabled = false;
						resetInputFields();
						Common.showToast(getApplicationContext(), "Account details updated.");
						changeOkayButton(false);
					}
					else{
						Common.showToast(getApplicationContext(), "Please input valid details.");
					}					
				}
				break;

			default:
				break;
			}

		}
	};
	

	@Override
	public void onAccountEditClicked(String number, String name,
			String ifsc) {
		editFunctionalityEnabled = true; 
		oldAccountNumber = number;
		this.accountName.setText(name);
		this.accountNumber.setText(number);
		this.accountIFSC.setText(ifsc);
		changeOkayButton(true);
		
	}
	
	private void changeOkayButton(boolean isEdit){
		if(isEdit){
			this.buttonOkay.setImageDrawable(getResources().getDrawable(R.drawable.button_done));
		}
		else{
			this.buttonOkay.setImageDrawable(getResources().getDrawable(R.drawable.button_add_new_account));
		}
	}

	@Override
	public boolean onOptionsItemSelected(final MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			super.onBackPressed();
			slideInLeft();
			return true;

		default:
			break;
		}
		return super.onOptionsItemSelected(item);
	}
	
	@Override
	public void onBackPressed(){
		super.onBackPressed();
		slideInLeft();
	}
	
	public void slideInLeft(){
		overridePendingTransition(R.anim.activity_slide_in_left, R.anim.activity_slide_out_left);
	}


	

}
