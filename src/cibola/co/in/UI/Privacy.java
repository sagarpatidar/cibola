/**
 *	Privacy.java
 *  Created by Sagar Patidar on 13-Aug-2014, 12:04:03 AM
 *	Copyright � 2014 by Cibola Pvt. Ltd
 *	All rights reserved.
 */

package cibola.co.in.UI;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.MenuItem;
import android.widget.TextView;
import cibola.co.in.General.Common;
import cibola.co.in.General.Constants;
import cibola.co.in.UI.R;

public class Privacy extends ActionBarActivity{
	
	@Override
	public void onCreate(Bundle savedInstanceState){
	    super.onCreate(savedInstanceState);
	    setContentView(R.layout.fragment_rbi);
	    
		TextView title = (TextView) findViewById(R.id.drawer_fragment_title);
		TextView sub_title = (TextView) findViewById(R.id.drawer_fragment_subtitle);
		Common.setTextViewFontStyle(Constants.FONT_ROBOTO_REGULAR,getAssets(), title, sub_title);
		Common.setupActionBar(getSupportActionBar());
	}
	
	@Override
	public boolean onOptionsItemSelected(final MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			super.onBackPressed();
			slideInLeft();
			return true;

		default:
			break;
		}
		return super.onOptionsItemSelected(item);
	}
	
	@Override
	public void onBackPressed(){
		super.onBackPressed();
		slideInLeft();
	}
	
	public void slideInLeft(){
		overridePendingTransition(R.anim.activity_slide_in_left, R.anim.activity_slide_out_left);
	}

}
