/**
 *	ContactsAtomicUpdate.java
 *  Created by Sagar Patidar on 07-Aug-2014, 8:26:00 AM
 *	Copyright � 2014 by Cibola Pvt. Ltd
 *	All rights reserved.
 */

package cibola.co.in.SqliteManager;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.util.Log;

public class ContactsAtomicUpdate {

	private List<ContentValues> insertValues;
	private List<ContentValues> updateValues;
	private List<String> deleteValues;
	private List<ContentValues> rawUpdateValues;
	private Database db ;
	
	public ContactsAtomicUpdate(Context context){
		insertValues = new ArrayList<ContentValues>();
		updateValues = new ArrayList<ContentValues>();
		deleteValues = new ArrayList<String>();
		rawUpdateValues = new ArrayList<ContentValues>();
		db = new Database(context);
	}
	
	public void commit(){
		//is to be done atomicly.
		// as of now I am implementing it in non-atomic fashion so at that least it works in best case scenario.

		//getContext().getContentResolver().notifyChange(uri, null);
		Log.d("UPDATE SIZE", insertValues.size()+" "+updateValues.size()+" "+deleteValues.size());
		for(ContentValues cv: insertValues){
			db.addContact(cv.getAsString(Tables.contact_mobile), cv.getAsString(Tables.contact_name), 
					cv.getAsString(Tables.contact_email),0);
		}
		
		for(ContentValues cv: updateValues){
			db.updateContactNameEmail(cv.getAsString(Tables.contact_mobile), cv.getAsString(Tables.contact_name), 
					cv.getAsString(Tables.contact_email));
		}
		
		for(String id: deleteValues){
			db.removeContact(id);
		}
		
		for(ContentValues cv: rawUpdateValues){
			db.addRawEntry(cv.getAsString(Tables.rawupdates_mobile), cv.getAsString(Tables.rawupdates_name),
					cv.getAsString(Tables.rawupdates_email), cv.getAsInteger(Tables.rawupdates_ack));
		}
		
		
	}
	
	public void addInsertValue(String opp_id,String name, String email){
		ContentValues cv = new ContentValues();
		cv.put(Tables.contact_mobile, opp_id);
		cv.put(Tables.contact_name, name);
		cv.put(Tables.contact_email, email);
		insertValues.add(cv);
		addRawUpdateValue(opp_id, name, name, 1);
	}
	
	public void addUpdateValue(String opp_id,String name, String email){
		ContentValues cv = new ContentValues();
		cv.put(Tables.contact_mobile, opp_id);
		cv.put(Tables.contact_name, name);
		cv.put(Tables.contact_email, email);
		if(db.isContactBlocked(opp_id)){
			cv.put(Tables.contact_is_blocked,0);
		}
		updateValues.add(cv);
		//addRawUpdateValue(opp_id, name, email, 2);
	}
	
	public void addDeleteID(String id){
		deleteValues.add(id);
		addRawUpdateValue(id, "", "", -1);
	}
	
	public void addRawUpdateValue(String mobile, String name, String email, int ack){
		ContentValues cv = new ContentValues();
		cv.put(Tables.rawupdates_mobile, mobile);
		cv.put(Tables.rawupdates_name, name);
		cv.put(Tables.rawupdates_email, email);
		cv.put(Tables.rawupdates_ack, ack);
		rawUpdateValues.add(cv);
	}
	
	
	
	
}
