package cibola.co.in.SqliteManager;

import java.util.HashMap;
import java.util.Random;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteException;
import android.net.Uri;
import android.util.Log;
import cibola.co.in.General.Common;
import cibola.co.in.Graphics.AlphabetImage;

public class Database 
{

	DataProvider dataProvider;
	private Context mContext;
	public Database(Context context)
	{
		dataProvider = new DataProvider();
		this.mContext = context;
	}

	/********************************************************************
	 * Contacts API
	 ********************************************************************/	
	public boolean addContact(String cid, String cName, String email, int isBlocked)
	{
		boolean result  = false;
		if(isContactExists(cid))
		{
			Log.d("CONTACT ALREADY EXISTS", "Contact '"+cid +"' already exists");
			return result;
		}
		ContentValues cv = new ContentValues();
		cv.put(Tables.contact_mobile, cid);
		cv.put(Tables.contact_name, cName);
		cv.put(Tables.contact_email, email);
		cv.put(Tables.contact_is_blocked, isBlocked);
		cv.put(Tables.contact_background, AlphabetImage.palatte[new Random().nextInt(AlphabetImage.palatte.length)]);
		cv.put(Tables.contact_timestamp, (new java.util.Date()).getTime());

		try
		{
			Uri uri = this.mContext.getContentResolver().insert(DataProvider.URI_CONTACTS, cv);
			if(uri!=null){
				result = true;
			}
		}
		catch(SQLiteException e)
		{
			Log.d("INSERTION FAILD", "Unable to add contact : "+cid);
			e.printStackTrace();
		}
		return result;
	}

	private boolean isContactExists(String cid){
		boolean result = false;
		Cursor cursor = this.mContext.getContentResolver().query(DataProvider.URI_CONTACTS, new String[]{Tables.contact_mobile}, 
				Tables.contact_mobile+"=?", new String[]{cid+""}, null);

		if(cursor!=null){
			if(cursor.getCount()>0){
				result = true;
			}
			cursor.close();
		}
		
		return result;
	}

	public boolean updateInviteStatus(String cid, int status){
		boolean result = false;
		ContentValues cv = new ContentValues();		
		cv.put(Tables.contact_is_invited, status);
		long numRowsAffected = this.mContext.getContentResolver().update(DataProvider.URI_CONTACTS, cv, Tables.contact_mobile+"=? ", new String[]{cid+""});
		if(numRowsAffected>0){
			result = true;
		}
		return result;
	}
	
	public boolean updateAllread(String cid){
		boolean result = false;
		ContentValues cv = new ContentValues();		
		cv.put(Tables.contact_unread_transaction_count, 0);
		long numRowsAffected = this.mContext.getContentResolver().update(DataProvider.URI_CONTACTS, cv, Tables.contact_mobile+"=? ", new String[]{cid+""});
		if(numRowsAffected>0){
			result = true;
		}
		return result;
	}
	
	public boolean updateLatestTransaction(String cid, int to_from, int send_req, int amount, String comment, long timestamp){
		boolean result = false;
		ContentValues cv = new ContentValues();
		cv.put(Tables.contact_to_from, to_from);
		cv.put(Tables.contact_send_req, send_req);
		cv.put(Tables.contact_amount, amount);
		cv.put(Tables.contact_comment, comment);
		//cv.put(Tables.contact_unread_transaction_count, 1);
		cv.put(Tables.contact_timestamp, timestamp);
		int  totalTransactionCount = -1;
		int unreadCount = 0;
		Cursor cursor = this.mContext.getContentResolver().query(DataProvider.URI_CONTACTS, 
				new String[]{Tables.contact_total_transaction_count, Tables.contact_unread_transaction_count}, 
				Tables.contact_mobile+" = ? ", new String[]{cid}, null);
		if(cursor!=null){
			if(cursor.moveToFirst()){
				totalTransactionCount = cursor.getInt(cursor.getColumnIndex(Tables.contact_total_transaction_count));
				unreadCount = cursor.getInt(cursor.getColumnIndex(Tables.contact_unread_transaction_count));				
			}
			cursor.close();
		}
		long numRowsAffected  = -1;
		if(totalTransactionCount>=0){
			cv.put(Tables.contact_total_transaction_count, totalTransactionCount+1);
			if(to_from==0){
				unreadCount++;
				cv.put(Tables.contact_unread_transaction_count, unreadCount);
			}
			numRowsAffected = this.mContext.getContentResolver().update(DataProvider.URI_CONTACTS, cv, Tables.contact_mobile+"=? ", new String[]{cid+""});
		}
		else{
			System.out.println("Something went wrong. Unable to read total transaction count!");
		}
		if(numRowsAffected>0){
			result = true;
		}
		return result;
	}

	public boolean updateContactNameEmail(String cid,String name, String email)
	{
		boolean result = false;
		ContentValues cv = new ContentValues();		
		cv.put(Tables.contact_name, name);
		cv.put(Tables.contact_email, email);
		long numRowsAffected = this.mContext.getContentResolver().update(DataProvider.URI_CONTACTS, cv, Tables.contact_mobile+"=? ", new String[]{cid});
		if(numRowsAffected>0){
			result = true;
		}
		return result;
	}

	public boolean updateProfilePicture(String cid,long fileID, String filePath)
	{
		boolean result = false;
		ContentValues cv = new ContentValues();		
		cv.put(Tables.contact_image_id, fileID);
		cv.put(Tables.contact_image_local_filepath, filePath);
		long numRowsAffected = this.mContext.getContentResolver().update(DataProvider.URI_CONTACTS, cv, Tables.contact_mobile+"=? ", new String[]{cid});
		if(numRowsAffected>0){
			result = true;
		}
		return result;
	}

	public boolean updateContactTimestamp(String cid)
	{
		boolean result = false;
		ContentValues cv = new ContentValues();		
		cv.put(Tables.contact_timestamp, (new java.util.Date()).getTime());
		long numRowsAffected = this.mContext.getContentResolver().update(DataProvider.URI_CONTACTS, cv, Tables.contact_mobile+"=? ", new String[]{cid});
		if(numRowsAffected>0){
			result = true;
		}
		return result;
	}

	public boolean updateOnCibolaStatus(String cid, int status){
		boolean result = false;
		ContentValues cv = new ContentValues();
		cv.put(Tables.contact_onCibola, status);
		long numRowsAffected = this.mContext.getContentResolver().update(DataProvider.URI_CONTACTS, cv, Tables.contact_mobile+"=? ", new String[]{cid});
		if(numRowsAffected>0){
			result = true;
		}
		return result;
	}
	
	public ContactsCursorAndHashMap getAllContacts()
	{
		HashMap<String, Integer> hashMap = new HashMap<String, Integer>();
		Cursor cursor = this.mContext.getContentResolver().query(DataProvider.URI_CONTACTS, new String[]{
				Tables.contact_mobile, Tables.contact_email, Tables.contact_name, Tables.contact_image_id
		}, null, null, null);
		if(cursor!=null){
			if(cursor.moveToFirst()){
				int row = 0;
				while(!cursor.isAfterLast()){
					String mobile = cursor.getString(cursor.getColumnIndex(Tables.contact_mobile));
//					System.out.println("Contact:- mobile-"+mobile+" row-"+row);
					hashMap.put(mobile, row);
					cursor.moveToNext();
					row++;
				}
			}
		}
		cursor.moveToFirst();
		return new ContactsCursorAndHashMap(hashMap, cursor);
	}

	
	public int getBackgroundColor(String mobile){
		
		Cursor cursor = this.mContext.getContentResolver().query(DataProvider.URI_CONTACTS, new String[]{Tables.contact_background}, 
				Tables.contact_mobile+"=?", new String[]{mobile}, null);
		if(cursor!=null && cursor.moveToFirst()){
			int color = cursor.getInt(cursor.getColumnIndex(Tables.contact_background));
			return color;
		}
		return 0;
	}
	
	public boolean getOnCibolaStatus(String mobile){
		
		Cursor cursor = this.mContext.getContentResolver().query(DataProvider.URI_CONTACTS, new String[]{Tables.contact_onCibola}, 
				Tables.contact_mobile+"=?", new String[]{mobile}, null);
		if(cursor!=null && cursor.moveToFirst()){
			int onCibola = cursor.getInt(cursor.getColumnIndex(Tables.contact_onCibola));
			if(onCibola==1){
				return true;
			}
		}
		return false;
	}
	
	public boolean isContactBlocked(String mobile){
		
		Cursor cursor = this.mContext.getContentResolver().query(DataProvider.URI_CONTACTS, new String[]{Tables.contact_is_blocked}, 
				Tables.contact_mobile+"=?", new String[]{mobile}, null);
		if(cursor!=null && cursor.moveToFirst()){
			int color = cursor.getInt(cursor.getColumnIndex(Tables.contact_is_blocked));
			if(color==1){
				return true;
			}
		}
		return false;
	}
	
	public String getName(String mobile){
		Cursor cursor = this.mContext.getContentResolver().query(DataProvider.URI_CONTACTS, new String[]{Tables.contact_name}, 
				Tables.contact_mobile+"=?", new String[]{mobile}, null);
		if(cursor!=null && cursor.moveToFirst()){
			return cursor.getString(cursor.getColumnIndex(Tables.contact_name));
		}
		return mobile;
	}
	
	public String getFileUrl(String mobile){
		Cursor cursor = this.mContext.getContentResolver().query(DataProvider.URI_CONTACTS, new String[]{Tables.contact_image_local_filepath}, 
				Tables.contact_mobile+"=?", new String[]{mobile}, null);
		if(cursor!=null && cursor.moveToFirst()){
			return cursor.getString(cursor.getColumnIndex(Tables.contact_image_local_filepath));
		}
		return null;
	}
	
	

	public boolean removeContact(String opp_id){
		boolean result = false;
		long numRowsAffected = this.mContext.getContentResolver().delete(DataProvider.URI_CONTACTS, Tables.contact_mobile+"=? ", new String[]{opp_id});
		if(numRowsAffected>0){
			result = true;
		}
		return result;
	}
	
	public boolean removeAllContacts(){
		boolean result = false;
		long numRowsAffected = this.mContext.getContentResolver().delete(DataProvider.URI_CONTACTS, null, null);
		if(numRowsAffected>0){
			result = true;
		}
		return result;
	}
	
	/********************************************************************
	 * Transaction API
	 ********************************************************************/	

	public boolean addNewTransaction(String transaction_id, String opp_id, int to_from, int send_req, int amount, String comment,int isDate){
		boolean result = false;
		try
		{
			long timestamp = new java.util.Date().getTime();
			if(transaction_id.length()>10){
				String tmp = transaction_id.substring(6);
				tmp = tmp.substring(0, tmp.length()-4);
				try{
					timestamp = Long.parseLong(tmp);
				}
				catch(Exception e){
					e.printStackTrace();
				}
			}
			Uri uri = this.mContext.getContentResolver().insert(DataProvider.URI_TRANSACTIONS, Common.createCVtransaction(transaction_id, opp_id, to_from, send_req, amount, comment, isDate, timestamp));
			boolean isLatestTransactionUpdated = updateLatestTransaction(opp_id, to_from, send_req, amount, comment, timestamp); 
			if(uri!=null && isLatestTransactionUpdated){
				return true;
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return result;

	}
	
	public boolean updateTransactionStatus(String transactionID, int status){
		boolean result = false;
		ContentValues cv = new ContentValues();
		cv.put(Tables.trans_status, status);
		long numRowsAffected = this.mContext.getContentResolver().update(DataProvider.URI_TRANSACTIONS, cv, Tables.trans_transaction_id+"=? ", new String[]{transactionID});
		if(numRowsAffected>0){
			result = true;
		}
		return result;
	}
	
	public boolean isTransactionTableEmpty(){
		Cursor cursor = this.mContext.getContentResolver().query(DataProvider.URI_TRANSACTIONS, new String[]{Tables.trans_serial}, 
				null, null, null);
		if(cursor!=null){
			if(cursor.getCount()>0){
				return false;
			}
		}
		return true;
	}
	
	public int getTotalChatCount(String contactMobile){
		Cursor cursor = this.mContext.getContentResolver().query(DataProvider.URI_TRANSACTIONS, new String[]{Tables.trans_serial}, 
				Tables.trans_opponent_id+"=?", new String[]{contactMobile}, null);
		if(cursor!=null){
			return cursor.getCount();
		}
		return 0;
	} 

	/********************************************************************
	 * RawDatabase Updates
	 ********************************************************************/	

	public boolean addRawEntry(String opp_id,String name, String email, int ack){
		boolean result  = false;
		
		if(isRawEntryExists(opp_id)){
			removeRawEntry(opp_id+"");
		}
		
		try
		{
			Uri uri = this.mContext.getContentResolver().insert(DataProvider.URI_RAWUPDATES, Common.createRawUpdateValues(opp_id, name, email, ack));
			if(uri!=null){
				result = true;
			}
		}
		catch(SQLiteException e)
		{
			e.printStackTrace();
		}
		return result;

	}

	private boolean isRawEntryExists(String mobile){
		boolean result = false;
		Cursor cursor = this.mContext.getContentResolver().query(DataProvider.URI_RAWUPDATES, new String[]{Tables.rawupdates_serial}, 
				Tables.rawupdates_mobile+"=?", new String[]{mobile+""}, null);

		if(cursor!=null){
			if(cursor.getCount()>0){
				result = true;
			}
			cursor.close();
		}
		
		return result;
	}
	
	public Cursor getAllRawEntry(){
		Cursor cursor = mContext.getContentResolver().query(DataProvider.URI_RAWUPDATES, null, null, null, null);
		return cursor;
	}
	
	public boolean removeRawEntry(String id){
		boolean result = false;

		if(isAccountExists(id)){
			long numRowsAffected = this.mContext.getContentResolver().delete(DataProvider.URI_RAWUPDATES, 
					Tables.rawupdates_mobile+"=? ", new String[]{id});
			if(numRowsAffected>0){
				result = true;
			}
		}
		return result;
	}

	public boolean removeAllRawEntry(){
		boolean result = false;
		long numRowsAffected = this.mContext.getContentResolver().delete(DataProvider.URI_RAWUPDATES,null, null);
		if(numRowsAffected>0){
			result = true;
		}
		return result;
	}
	
	
	/********************************************************************
	 * Bank Accounts
	 ********************************************************************/

	public boolean addAccount(String accountNumber, String accountName, String accountIFSC){
		boolean result  = false;
		if(isAccountExists(accountNumber)){
			return result;
		}
		ContentValues cv = new ContentValues();
		cv.put(Tables.account_number, accountNumber);
		cv.put(Tables.account_name, accountName);
		cv.put(Tables.account_ifsc, accountIFSC);
		cv.put(Tables.account_timestamp, new java.util.Date().getTime());
		try
		{
			Uri uri = this.mContext.getContentResolver().insert(DataProvider.URI_BANK_ACCOUNTS, cv);
			if(uri!=null){
				result = true;
			}
		}
		catch(SQLiteException e)
		{
			e.printStackTrace();
		}
		return result;

	}

	private boolean isAccountExists(String accountID){
		boolean result = false;
		Cursor cursor = this.mContext.getContentResolver().query(DataProvider.URI_BANK_ACCOUNTS, new String[]{Tables.account_number}, 
				Tables.account_number+"=?", new String[]{accountID}, null);

		if(cursor!=null){
			if(cursor.getCount()>0){
				result = true;
			}
			cursor.close();
		}
		return result;
	}

	public boolean removeAccount(String accountID){
		boolean result = false;

		if(isAccountExists(accountID)){
			long numRowsAffected = this.mContext.getContentResolver().delete(DataProvider.URI_BANK_ACCOUNTS, 
					Tables.account_number+"=? ", new String[]{accountID});
			if(numRowsAffected>0){
				result = true;
			}
			
			Log.d("DEBUG",accountID+" exists!");
		}
		else{
			Log.d("DEBUG",accountID+" doesn't exist!");
		}
		return result;
	}

	public boolean updateAccount(String accountNumber, String accountName, String IFSC){
		boolean result = false;
		ContentValues cv = new ContentValues();
		cv.put(Tables.account_name, accountName);
		cv.put(Tables.account_ifsc, IFSC);
		long numRowsAffected = this.mContext.getContentResolver().update(DataProvider.URI_BANK_ACCOUNTS, cv, Tables.account_number+"=? ", new String[]{accountNumber});
		if(numRowsAffected>0){
			result = true;
		}
		return result;
	}
	
	public int getTotalAccountsCount(){
		Cursor cursor = this.mContext.getContentResolver().query(DataProvider.URI_BANK_ACCOUNTS, new String[]{Tables.account_serial}, null	, null, Tables.contact_timestamp+" DESC ");
		if(cursor!=null){
			return cursor.getCount();
		}
		return -1;
	}

}
