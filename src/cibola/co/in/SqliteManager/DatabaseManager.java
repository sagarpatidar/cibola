package cibola.co.in.SqliteManager;

import java.io.File;

import android.content.ContentValues;
import android.content.Context;
import android.content.ContextWrapper;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

public class DatabaseManager {

	private SQLiteDatabase database;
	private DBconnector dbconnector ;

	
	public DatabaseManager(Context context)
	{
		dbconnector = new DBconnector(context);		
		open();
		
	}

	protected void finalize()
	{
		close();
	}
	
	public SQLiteDatabase getDatabase() {
		return database;
	}

	public void setDatabase(SQLiteDatabase database) {
		this.database = database;
	}
	
	public void open() throws SQLException
	{
		setDatabase(dbconnector.getWritableDatabase());
	}
	
	public void close()
	{
		database.close();
	}

	@SuppressWarnings("unused")
	private static boolean doesDatabaseExist(ContextWrapper context, String dbName)
	{
		File dbFile=context.getDatabasePath(dbName);
		return dbFile.exists();
	}
	
	public Cursor query(String tableName, String[] projection, String selection,
			String[] selectionArgs, String sortOrder) 
	{
		return database.query(tableName, projection, selection, selectionArgs, null, null, sortOrder);

	}
	
	public long insertQuery(String tablename, ContentValues cv)
	{
		return database.insert(tablename,null,cv);
	}
	
	public int updateQuery(String tablename, ContentValues values, String selection,
			String[] selectionArgs)
	{
		return database.update(tablename, values, selection, selectionArgs);
	}
	
	public int delete(String tablename, String selection, String[] selectionArgs){
		return database.delete(tablename, selection, selectionArgs);
	}
	
	public Cursor rawQuery(String sql, String[] selectionArgs){
		return database.rawQuery(sql, selectionArgs);
	}
	
}
