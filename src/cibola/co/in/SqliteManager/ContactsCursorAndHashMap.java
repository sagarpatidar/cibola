/**
 *	ContactsCursorAndHashMap.java
 *  Created by Sagar Patidar on 07-Aug-2014, 12:53:56 AM
 *	Copyright � 2014 by Cibola Pvt. Ltd
 *	All rights reserved.
 */

package cibola.co.in.SqliteManager;

import java.util.HashMap;

import android.database.Cursor;

public class ContactsCursorAndHashMap {

	private HashMap<String, Integer> rowMapping;
	private Cursor localContactsCursor;
	public ContactsCursorAndHashMap(HashMap<String, Integer> hashMap, Cursor c){
		this.rowMapping = hashMap;
		this.localContactsCursor = c;
	}
	
	public Cursor getLocalContactsCursor() {
		return localContactsCursor;
	}
	
	public void setLocalContactsCursor(Cursor localContactsCursor) {
		this.localContactsCursor = localContactsCursor;
	}
	
	public HashMap<String, Integer> getRowMapping() {
		return rowMapping;
	}
	
	public void setRowMapping(HashMap<String, Integer> rowMapping) {
		this.rowMapping = rowMapping;
	}
}