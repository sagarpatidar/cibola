/**
 *	Constants.java
 *  Created by Sagar Patidar on 03-Aug-2014, 1:42:11 PM
 *	Copyright � 2014 by Cibola Pvt. Ltd
 *	All rights reserved.
 */

package cibola.co.in.General;

public class Constants {
	public static final String FONT_ROBOTO_REGULAR = "fonts/roboto_regular.ttf";
	public static final String FONT_ROBOTO_LIGHT = "fonts/roboto_light.ttf";
	public static final String FONT_ROBOTO_MEDIUM = "fonts/roboto_medium.ttf";
	//public static final String FONT_SEOGEUI_SEMILIGHT = "fonts/segoeui_semilight.ttf";
	
	//public static final String FONT_SEOGEUI_BOLD = "fonts/segoeui_bold.ttf";
	
	//Mixpanel
	public static final String MIXPANEL_TOKEN = "af5314f2811548486c0b441a1bbace37";
	
	public static final String CONTACT_NAME = "contact_name";
	public static final String CONTACT_ID = "contact_id";
	public static final String CONTACT_BACKGROUND = "contact_background";
	public static final String CIBOLA_MAIN_IMAGE_DIR = "/Cibola/Images";
	public static final String CIBOLA_MEDIUM_IMAGE_DIR = "/Cibola/Images/medium";
	public static final String CIBOLA_LARGE_IMAGE_DIR = "/Cibola/Images/large";
	public static final String CIBOLA_USER_PROFILE_IMAGE_FILEPATH = CIBOLA_MAIN_IMAGE_DIR+ "/profile.png";
	
	public static final int PROFILE_COVER_OPACITY = 25;
	
	public static final String OTP_BROADCAST_MESSAGE = "otp_broadcast";
	public static final String OTP_ACTION = "cibola.co.in.OTP_MESSAGE";
	public static final String CONTACTS_UPDATE_COMPLETE_BROADCAST_MESSAGE = "contacts_update_complete";
	public static final String CONTACTS_UPDATE_COMPLETE_BROADCAST_ACTION = "cibola.co.in.contacts_update_complete";
	
	//Notifications
	public static final int NOTIFICATION_LOCAL_CODE = 2;
	public static final String NOTIFY_CODE = "code";
	public static final int NOTIFY_TXN_CODE = 101;
	public static final String NOTIFY_TXN_AMOUNT = "amount";
	public static final String NOTIFY_TXN_COMMENT = "comment";
	public static final String NOTIFY_TXN_MOBILE = "mobile";
	public static final String NOTIFY_TXN_IS_REQUESTED = "is_requested";
	public static final String NOTIFY_TXN_ID = "trans_id";
	public static final String NOTIFICATION_CLEAR = "clear";
	// Communication via Intents between activities
	public static final int CONTACTS_SERVICE_EXECUTION_CODE = 100;
	public static final String CONTACTS_MANAGER_IS_INIT ="contacts_init";
	public static final String TRANSACTION_IS_REQUESTED = "isRequested";
	public static final String INTENT_CONTACT_NUMBER = "mobile";
	public static final String INTENT_CONTACT_NAME = "name";
	public static final String INTENT_FILE_PATH = "filepath";
	public static final String INTENT_COMMENT_DIALOG_DISMISSED = "comment_dialog_dismissed";
	public static final String INTENT_LARGE_IMAGE_DOWNLOAD_COMPLETE = "large_image_download_complete";
	public static final String INTENT_UPDATE_BALANCE = "amount";
	//Debugging
	public static final boolean DEBUG_MODE = true;
	public static final String BUG = "BUG";
	public static final String DEBUG = "DEBUG";
	
	//Web connection
	public static final String DOMAIN_NAME = "http://www.cibola.in/";
	public static final String IMAGE_SERVER_MED_SIZE = DOMAIN_NAME+"media/";
	public static final String IMAGE_SERVER_LARGE_SIZE = DOMAIN_NAME+"media/";
	public static final String GCM_SENDER_ID = "827132571191";

	public static final String WEB_RESPONSE_SUCCESS = "SUCCESS";
	public static final String WEB_RESPONSE_MESSAGE = "MESSAGE";
	public static final String WEB_RESPONSE_FIRSTNAME = "FIRST_NAME";
	public static final String WEB_RESPONSE_LASTNAME = "LAST_NAME";
	public static final String WEB_RESPONSE_EMAIL = "EMAIL";
	public static final String WEB_RESPONSE_DATA = "DATA";
	public static final String WEB_RESPONSE_TRANSACTION_ID = "TRANSACTION_ID";
	public static final String WEB_RESPONSE_ON_CIBOLA = "ON_CIBOLA";
	public static final String WEB_RESPONSE_FILEID = "FILE_ID";
	public static final String WEB_RESPONSE_FILE_PATH = "FILE_PATH";
	public static final String WEB_RESPONSE_CONTACT_NUMBER = "CONTACT_NUMBER";
	
	//post variables
	public static final String CONACTS_SYNC = "sync_contacts";
	public static final String WEB_USERNAME = "username";
	public static final String WEB_MOBILE = "mobile";
	public static final String WEB_SIGNUP_GENDER = "gender";
	public static final String WEB_SIGNUP_DOB = "dob";
	public static final String WEB_UPLOAD_IMAGE = "image";
	public static final String WEB_VERIFICATION_CODE = "verify_code";
	
	public static final String WEB_SYNC_CONTACTS_NAME = "NAME";
	public static final String WEB_SYNC_CONTACTS_EMAIL = "EMAIL";
	public static final String WEB_SYNC_CONTACTS_MOBILE = "MOBILE";
	public static final String WEB_SYNC_CONTACTS_ACK = "ACK";
	public static final String WEB_SYNC_CONTACTS_FILE_ID = "FILE_ID";
	public static final String WEB_SYNC_CONTACTS_SUPERUSER = "USERNAME";
	public static final String WEB_SYNC_CONTACTS_CONTACTS = "CONTACTS";
	
	public static final String WEB_CONTACTS_INVITE = "invite";
	
	public static final String WEB_TRANSACTION_TO_USERNAME = "to_id";
	public static final String WEB_TRANSACTION_FROM_USERNAME = "from_id";
	public static final String WEB_TRANSACTION_AMOUNT = "amount";
	public static final String WEB_TRANSACTION_COMMENT = "comment";
	public static final String WEB_TRANSACTION_ID = "tid";
	public static final String WEB_TRANSACTION_IS_REQUESTED = "is_requested";
	
	public static final String WEB_LOGIN_USERNAME = "username";
	public static final String WEB_LOGIN_PASSWORD = "password";
	
	public static final String WEB_UPDATE_BALANCE = "amount";
	public static final String WEB_UPDATE_FIRST_NAME = "first_name";
	public static final String WEB_UPDATE_LAST_NAME  = "last_name";
	public static final String WEB_UPDATE_EMAIL = "email";
	public static final String WEB_UPDATE_OLD_PWD = "old_password";
	public static final String WEB_UPDATE_NEW_PWD = "new_password";
	
	public static final String NETWORK_SERVICE_CODE = "network_service_code";
	public static final int NETWORK_SERVICE_CODE_GET_GCM = 100;
	public static final int NETWORK_SERVICE_CODE_REGISTER_GCM_ON_CIBOLA = 101;
	public static final int NETWORK_SERVICE_CODE_TRANSACTION = 102;
	public static final int NETWORK_SERVICE_CODE_INVITE = 103;
	public static final int NETWORK_SERVICE_CODE_UPDATE_NAME = 104;
	public static final int NETWORK_SERVICE_CODE_UPDATE_EMAIL = 105;
	public static final int NETWORK_SERVICE_CODE_UPDATE_PASSWORD = 106;
	public static final int NETWORK_SERVICE_CODE_UPDATE_PIC = 107;
	public static final int NETWORK_SERVICE_CODE_DOWNLOAD_LARGE = 108;
	public static final int NETWORK_SERVICE_CODE_UPDATE_BALANCE = 109;
	
	public static final String INVITE_CONTACT_NUMBER = "contactNumber";
	public static final String WEB_GCM_REG_ID = "gcm_id";
	
	public static final String NOTIFICATION = "notification";
	
	//PAYTM
	public static final String PAYTM_MERCHANT_ID = "Cibola15869377551156";
	public static final String PAYTM_MERCHANT_KEY = "NxpQV5g6zDqN6%0d";
	public static final String PAYTM_CHANNEL_ID = "WAP";
	public static final String PAYTM_WEBSITE_NAME = "cibola";
	public static final String PAYTM_INDUSTRY_TYPE_ID = "Retail";
	public static final String PAYTM_THEME = "javas";
	
}
