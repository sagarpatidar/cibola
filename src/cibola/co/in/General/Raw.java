package cibola.co.in.General;

import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.util.DisplayMetrics;
import android.util.Log;
import android.provider.ContactsContract;
import android.provider.Contacts.People;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import cibola.co.in.SqliteManager.DataProvider;
import cibola.co.in.SqliteManager.Database;
import cibola.co.in.SqliteManager.Tables;
import cibola.co.in.others.ContactsManager;
import cibola.co.in.others.PhoneContactPair;

public class Raw {
	

	
	public static void insertTest(Context c, Activity activity){
//		ContactsManager manager = new ContactsManager(c);
//		try {
//			String response = manager.getInitContactList(c);
//			
//		} catch (JSONException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		/*for(int i=1;i<10000;i++){
			Log.d("Progress",i+"");
			ContentValues values = new ContentValues();
	        values.put(People.NUMBER, String.valueOf(i*1000));
	        values.put(People.TYPE, Phone.TYPE_CUSTOM);
	        values.put(People.LABEL, "testing: "+i);
	        values.put(People.NAME, "testing: "+i);
	        Uri dataUri = c.getContentResolver().insert(People.CONTENT_URI, values);
	        Uri updateUri = Uri.withAppendedPath(dataUri, People.Phones.CONTENT_DIRECTORY);
	        values.clear();
	        values.put(People.Phones.TYPE, People.TYPE_MOBILE);
	        values.put(People.NUMBER, String.valueOf(i*1000));
	        updateUri = c.getContentResolver().insert(updateUri, values);
		}*/
		
		
	}
	
	

	public static void raw(Context c,Activity activity) throws JSONException{
//		Raw.insertRawContacts(activity);
		//Raw.addRawTransactions(c);
//		Database db = new Database(c);
//		db.updateContactNameEmail(9899637620L,	 "S. Patidar", "blah@blah.com");
//		db.updateOnCibolaStatus(9899637620L, 1);
//		db.updateOnCibolaStatus(9873803932L, 1);
		
		
//		ContactsManager manager = new ContactsManager();
//		manager.getAllNativeContactsQuick(activity);
//		Log.d("JSON OUTPUT",json);
//		Request req = new Request("http://54.191.38.64/cibola/testing/");
//		req.addParam("sync_contacts", json);
		//ServerConnector server = new ServerConnector(req);
		//server.start();
		//Raw.testServer();
	}
	
	public static void printContactsCursor(Cursor cursor)
	{
		
		if(cursor!=null){
			if(cursor.moveToFirst()){
				while(!cursor.isAfterLast()){
					System.out.println(cursor.getLong(0)+" "+cursor.getString(1)+" "+cursor.getInt(2)+" "+cursor.getInt(3)+" "+cursor.getLong(4));
					cursor.moveToNext();
				}
			}
		}
	}
	

	
	public static void jsonTesting(){
		JSONObject obj = new JSONObject();
		JSONArray jArray = new JSONArray();
		for(int i =0;i<3;i++){
			JSONObject user = new JSONObject();
			try {
				user.put("INSERT", 1);
				user.put("DELETE", 1);
				user.put("UPDATE", 1);
				user.put("EMAIL", "patidar.sagar6955@gmail.com");
				user.put("MOBILE", 9899637620L);
				user.put("NAME", "Sagar Patidar");
				
				
			} catch (JSONException e) {
				e.printStackTrace();
			}
			jArray.put(user);
		}
		
		try {
			obj.put("CONTACTS", jArray);
			obj.put("SUPERUSER", "yosagar");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		Log.d("JSON STRING:- ",obj.toString());
	} 
	
	public static void printPhoneContacts(PhoneContactPair pair){
		HashMap<String, String> map = pair.getHmap();
		int i =0;
		for (HashMap.Entry<String, String> entry : map.entrySet()) {
			
		    Log.d("Contact#"+i+": "+entry.getKey(), entry.getValue());
		    i++;
		}
		System.out.println(pair.getJsonStringNumbers());
	}

	
	public static void insertRawContacts(Context context){
	/*	Database database = new Database(context);
		database.addContact(9899637620L, "Sagar Patidar", "abc@example.com");
		database.addContact(9873803932L, "Rhythm Gupta", "abc@example.com");
		database.addContact(7042113077L, "Misbah Ashraf", "abc@example.com");
		database.addContact(6342118077L, "Utkarsh Ankit", "abc@example.com");
		database.addContact(9899637610L, "Sagar Patidar", "abc@example.com");
		database.addContact(9873803922L, "Rhythm Gupta", "abc@example.com");
		database.addContact(7042113037L, "Misbah Ashraf", "abc@example.com");
		database.addContact(6342118047L, "Utkarsh Ankit", "abc@example.com");
	*/
	}
	
	public static void addRawTransactions(Context context){
		Database database = new Database(context);
		//database.addNewTransaction("34234", opp_id, to_from, send_req, amount, comment, isDate)
		database.addNewTransaction(Common.generateTransactionId(),"1234567890", 0, 1, 100, "I need it urgent bro.",0);
		database.addNewTransaction(Common.generateTransactionId(),"1234567890", 1, 0, 100, "I need it urgent bro.",0);
//		database.addNewTransaction(9873803932L, 0, 1, 100, null,0);
//		database.addNewTransaction(9873803932L, 1, 0, 100, null,0);
//		database.addNewTransaction(9873803932L, 0, 0, 100, "For the last night dinner.",0);
	}
	
	public static void testServer(){
		/*Request req = new Request();
		req.setURL("http://54.191.38.64/cibola/display/");
		req.addParam("id", "1");
		req.addParam("csrfmiddlewaretoken", "TfXnVowSk9Lti2gt1fsjXxMwoPnCGlHa");
		ServerConnector server = new ServerConnector(req);
		server.start();
		System.out.println("IS IN BACKGROUND?");*/
	}
	
	// Don't Use it: Very slow
	/*
	public String getAllNativeContacts( Activity activity) throws JSONException{
		Database db = new Database(activity);
		ContactsCursorAndHashMap localContacts = db.getAllContacts();
		hashMap = localContacts.getRowMapping();
		localContactsCursor = localContacts.getLocalContactsCursor();		
		checkedContacts = new HashMap<Long,Boolean>();
		atomic = new ContactsAtomicUpdate(activity);
		int count = 0;
		JSONObject jsonContactsDataToSend = new JSONObject();
		JSONArray jArray = new JSONArray();
		ContentResolver contentResolver = activity.getContentResolver();
		Cursor cursor = contentResolver.query(PHONE_CONTACTS_URI, null, null, null, null);
		if(cursor!=null){
			while(cursor.moveToNext()){
				System.out.println("COUNT = "+count);
				count++;
				int hasPhoneNumber = Integer.parseInt(cursor.getString(cursor.getColumnIndex(CONTACT_HAS_PHONE_NUMBER)));
				String cid = cursor.getString(cursor.getColumnIndex(CONTACT_ID));
				String name = cursor.getString(cursor.getColumnIndex( CONTACT_DISPLAY_NAME));

				Cursor emailCursor = contentResolver.query(EmailCONTENT_URI,    null, EMAIL_CONTACT_ID+ " = ?", new String[] { cid }, null);
				String email = "";
				if(emailCursor.moveToFirst()){
					while (!emailCursor.isAfterLast()) {
						email += emailCursor.getString(emailCursor.getColumnIndex(DATA))+",";
						emailCursor.moveToNext();
					}	
				}
				emailCursor.close();


				if(hasPhoneNumber>0){

					Cursor phoneCursor = contentResolver.query(PHONE_PHONENUMBER_URI, null, PHONE_CONTACT_ID + " = ?", 
							new String[] { cid }, null);

					//if(phoneCursor.moveToFirst()){
					while(phoneCursor.moveToNext()){

						String phoneNumber = phoneCursor.getString(phoneCursor.getColumnIndex(PHONE_NUMBER));
						String filteredPhoneNumber = phoneNumber.replaceAll("\\D+","");
						if(filteredPhoneNumber.length()==12 && filteredPhoneNumber.substring(0, 2).equals("91")){
							filteredPhoneNumber = filteredPhoneNumber.substring(2);
						}
						else if(filteredPhoneNumber.length()==11 && filteredPhoneNumber.subSequence(0, 1).equals("0")){
							filteredPhoneNumber = filteredPhoneNumber.substring(1);
						}

						if(filteredPhoneNumber.length()==10){
							JSONObject jsonContact = new JSONObject();
							jsonContact.put("NAME", name);
							jsonContact.put("MOBILE",filteredPhoneNumber);
							jsonContact.put("EMAIL", email);

							Integer position =  hashMap.get(Long.parseLong(filteredPhoneNumber));
							if(position==null){
								//new Insertion
								atomic.addInsertValue(Long.parseLong(filteredPhoneNumber), name, email);
								jsonContact.put("ACK", 1);

							}
							else{
								localContactsCursor.moveToPosition(position);
								checkedContacts.put(localContactsCursor.getLong(localContactsCursor.getColumnIndex(Tables.contact_id)), true);
								String localName = localContactsCursor.getString(localContactsCursor.getColumnIndex(Tables.contact_name));
								String localEmail = localContactsCursor.getString(localContactsCursor.getColumnIndex(Tables.contact_email));
								if(!name.equals(localName) || 
										!email.equals(localEmail) ){
									//update
									atomic.addUpdateValue(Long.parseLong(filteredPhoneNumber), name, email);
									jsonContact.put("ACK", 2);

								}
								else{
									// neutral
									jsonContact.put("ACK", 0);
								}
							}
							jArray.put(jsonContact);
						}
					}
					phoneCursor.close();
				}
			}
		}
		//check for deleted contacts
		for(Long key:hashMap.keySet()){
			Boolean isChecked = checkedContacts.get(key);
			if(isChecked==null){
				JSONObject jsonContact = new JSONObject();
				jsonContact.put("NAME", "");
				jsonContact.put("MOBILE",key);
				jsonContact.put("EMAIL", "");
				jsonContact.put("ACK", -1);
				jArray.put(jsonContact);
				atomic.addDeleteID(key);
			}
		}
		jsonContactsDataToSend.put("SUPERUSER", "rhythmgupta"); // to be updated
		jsonContactsDataToSend.put("CONTACTS",jArray);

		cursor.close();
		localContactsCursor.close();

		return jsonContactsDataToSend.toString();
	}

	*/
	
	
}
