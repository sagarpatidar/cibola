/**
 *	SessionManager.java
 *  Created by Sagar Patidar on 29-Aug-2014, 11:30:01 AM
 *	Copyright � 2014 by Cibola Technologies Pvt. Ltd
 *	All rights reserved.
 */

package cibola.co.in.General;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

public class MyAccountManager {
	
	private SharedPreferences sharedPreferences;

	// Shared Preferences Keys -- Constants
	public static final String SHARED_PREFERENCES = "CibolaPrefs";
	public static final String SHARED_PREFERENCES_VARIABLE_FIRSTNAME = "first_name";
	public static final String SHARED_PREFERENCES_VARIABLE_LASTNAME = "last_name";
	public static final String SHARED_PREFERENCES_VARIABLE_MOBILE = "mobile";
	public static final String SHARED_PREFERENCES_VARIABLE_EMAIL = "email";
	public static final String SHARED_PREFERENCES_VARIABLE_BALANCE = "balance";
	public static final String SHARED_PREFERENCES_VARIABLE_SIGNUP_STATUS = "signup_status";
	public static final String SHARED_PREFERENCES_VARIABLE_REG_ID_SAVED_ON_CLOUD = "reg_id_saved_on_cloud";
	public static final String SHARED_PREFERENCES_VARIABLE_IS_INITIALIZED = "is_initialized";
	public static final String SHARED_PREFERENCES_VARIABLE_HAS_GCM_ID = "has_gcm_id";
	public static final String SHARED_PREFERENCES_VARIABLE_GCM_ID = "gcm_id";
	public static final String SHARED_PREFERENCES_VARIABLE_GCM_ID_SAVED_ON_CIBOLA_SERVER = "gcm_id_saved_on_cibola_server";
	public static final String SHARED_PREFERENCES_VARIABLE_IS_PWD_SET = "is_pwd_set";
	public static final String SHARED_PREFERENCES_VARIABLE_IS_LOGGED_IN = "is_logged_in";
	public static final String SHARED_PREFERENCES_VARIABLE_GOT_VERIFICATION_CODE = "got_verification_code";
	public static final String SHARED_PREFERENCES_VARIABLE_VERIFICATION_CODE = "verification_code";
	public static final String SHARED_PREFERENCES_VARIABLE_NOTIFICATION_COUNT = "notification_count";

	public static final String SHARED_PREFERENCES_VARIABLE_FIRST_TIME_CONTACTS = "first_time_contacts";
	
	public MyAccountManager(Context context){
		sharedPreferences = context.getSharedPreferences(SHARED_PREFERENCES, Context.MODE_PRIVATE);
	}
	
	public boolean signIn(String firstname, String lastname, String email, String mobile){
		SharedPreferences.Editor editor = sharedPreferences.edit();
		editor.putString(SHARED_PREFERENCES_VARIABLE_FIRSTNAME, firstname);
		editor.putString(SHARED_PREFERENCES_VARIABLE_LASTNAME, lastname);
		editor.putString(SHARED_PREFERENCES_VARIABLE_EMAIL, email);
		editor.putString(SHARED_PREFERENCES_VARIABLE_MOBILE, mobile);
		if(editor.commit()){
			setIsLoggedIn(true);
			return true;
		}
		return false;
	}
	
	public boolean signOut(){
		SharedPreferences.Editor editor = sharedPreferences.edit();
		editor.remove(SHARED_PREFERENCES_VARIABLE_FIRSTNAME);
		editor.remove(SHARED_PREFERENCES_VARIABLE_LASTNAME);
		editor.remove(SHARED_PREFERENCES_VARIABLE_EMAIL);
		editor.remove(SHARED_PREFERENCES_VARIABLE_MOBILE);
		if(editor.commit()){
			setIsLoggedIn(false);
			return true;
		}
		return false;
	}
	
	public boolean addParameters(String[] key, String[] value){
		SharedPreferences.Editor editor = sharedPreferences.edit();
		if(key!=null && value!=null && key.length!=value.length){
			Log.d("#DEBUG","Length of key and value arrays must be same. And these arrays must not be null");
			return false;
		}
		for(int i=0;i<key.length;i++){
			editor.putString(key[i], value[i]);
		}
		if(editor.commit()){
			return true;
		}
		return false;
	} 
	
	public boolean editParameter(String key, String value){
		SharedPreferences.Editor editor = sharedPreferences.edit();
		editor.putString(key, value);
		return editor.commit();
	}
	
	
	public boolean isExist(String key){
		return sharedPreferences.contains(key);
	}
	
	
	public String getMobileNumber(){
		return sharedPreferences.getString(SHARED_PREFERENCES_VARIABLE_MOBILE, null);
	}
	
	public String getEmail(){
		return sharedPreferences.getString(SHARED_PREFERENCES_VARIABLE_EMAIL, null);
	}
	
	public String getUsername(){
		return getMobileNumber();
	}

	public String getFirstname(){
		return sharedPreferences.getString(SHARED_PREFERENCES_VARIABLE_FIRSTNAME, "welcome");
	}
	
	public String getLastname(){
		return sharedPreferences.getString(SHARED_PREFERENCES_VARIABLE_LASTNAME, "");
	}
	
	public String getFullname(){
		return sharedPreferences.getString(SHARED_PREFERENCES_VARIABLE_FIRSTNAME, "Welcome")+" "+
				sharedPreferences.getString(SHARED_PREFERENCES_VARIABLE_LASTNAME, "");
	}
	
	public int getBalance(){
		return sharedPreferences.getInt(SHARED_PREFERENCES_VARIABLE_BALANCE, 0);
	}
	
	public boolean getPwdSetStatus(){
		return sharedPreferences.getBoolean(SHARED_PREFERENCES_VARIABLE_IS_PWD_SET, false);
	}
	
	
	public boolean updateBalance(int remainingBalance){
		SharedPreferences.Editor editor = sharedPreferences.edit();
		editor.putInt(SHARED_PREFERENCES_VARIABLE_BALANCE, remainingBalance);
		return editor.commit();
	}
	
	public boolean updateSignupStatus(int status){
		SharedPreferences.Editor editor = sharedPreferences.edit();
		editor.putInt(SHARED_PREFERENCES_VARIABLE_SIGNUP_STATUS, status);
		return editor.commit();
	}
	
	public boolean isLoggedIn(){
		if(isExist(SHARED_PREFERENCES_VARIABLE_IS_LOGGED_IN)){
			return sharedPreferences.getBoolean(SHARED_PREFERENCES_VARIABLE_IS_LOGGED_IN, false);
		}
		return false;
	}
	
	public boolean setIsLoggedIn(boolean isLoggedIn){
		SharedPreferences.Editor editor = sharedPreferences.edit();
		editor.putBoolean(SHARED_PREFERENCES_VARIABLE_IS_LOGGED_IN, isLoggedIn);
		return editor.commit();
	}
	
	public boolean isInitialized(){
		if(isExist(SHARED_PREFERENCES_VARIABLE_IS_INITIALIZED)){
			return sharedPreferences.getBoolean(SHARED_PREFERENCES_VARIABLE_IS_INITIALIZED, false);
		}
		return false;
	}
	
	public boolean setIsInitialized(boolean isInitialized){
		SharedPreferences.Editor editor = sharedPreferences.edit();
		editor.putBoolean(SHARED_PREFERENCES_VARIABLE_IS_INITIALIZED, isInitialized);
		return editor.commit();
	}
	
	public boolean setIsPwdSet(boolean isPwdSet){
		SharedPreferences.Editor editor = sharedPreferences.edit();
		editor.putBoolean(SHARED_PREFERENCES_VARIABLE_IS_PWD_SET, isPwdSet);
		return editor.commit();
	}
	
	public boolean hasGCMId(){
		if(isExist(SHARED_PREFERENCES_VARIABLE_HAS_GCM_ID)){
			return sharedPreferences.getBoolean(SHARED_PREFERENCES_VARIABLE_HAS_GCM_ID, false);
		}
		return false;
	}
	
	public boolean setHasGCMId(boolean hasID){
		SharedPreferences.Editor editor = sharedPreferences.edit();
		editor.putBoolean(SHARED_PREFERENCES_VARIABLE_HAS_GCM_ID, hasID);
		return editor.commit();
	}
	
	public boolean setGCMId(String id){
		SharedPreferences.Editor editor = sharedPreferences.edit();
		editor.putString(SHARED_PREFERENCES_VARIABLE_GCM_ID, id);
		return editor.commit();
	}
	
	public boolean setBalance(int amount){
		SharedPreferences.Editor editor = sharedPreferences.edit();
		editor.putInt(SHARED_PREFERENCES_VARIABLE_BALANCE, amount);
		return editor.commit();
	}
	
	public String getGCMId(){
		return sharedPreferences.getString(SHARED_PREFERENCES_VARIABLE_GCM_ID, null);
	}

	
	public boolean isGCMRegisteredOnCiola(){
		if(isExist(SHARED_PREFERENCES_VARIABLE_REG_ID_SAVED_ON_CLOUD)){
			return sharedPreferences.getBoolean(SHARED_PREFERENCES_VARIABLE_REG_ID_SAVED_ON_CLOUD, false);
		}
		return false;
	}
	
	public boolean setIsGCMRegisteredOnCibola(boolean hasID){
		SharedPreferences.Editor editor = sharedPreferences.edit();
		editor.putBoolean(SHARED_PREFERENCES_VARIABLE_REG_ID_SAVED_ON_CLOUD, hasID);
		return editor.commit();
	}
	
	public boolean isRecievedVerificationCode(){
		return sharedPreferences.getBoolean(SHARED_PREFERENCES_VARIABLE_GOT_VERIFICATION_CODE, false);
	}
	
	public boolean setIsRecievedVerificationCode(boolean isRecievedVerificationCode){
		SharedPreferences.Editor editor = sharedPreferences.edit();
		editor.putBoolean(SHARED_PREFERENCES_VARIABLE_GOT_VERIFICATION_CODE, isRecievedVerificationCode);
		return editor.commit();
	}
	
	public boolean setVerificationCode(int code){
		SharedPreferences.Editor editor = sharedPreferences.edit();
		editor.putInt(SHARED_PREFERENCES_VARIABLE_VERIFICATION_CODE, code);
		editor.putBoolean(SHARED_PREFERENCES_VARIABLE_GOT_VERIFICATION_CODE, true);
		return editor.commit();
	}
	
	public int getVerificationCode(){
		return sharedPreferences.getInt(SHARED_PREFERENCES_VARIABLE_VERIFICATION_CODE, -1);
	}
	
	public String getFirstTimeContacts(){
		return sharedPreferences.getString(SHARED_PREFERENCES_VARIABLE_FIRST_TIME_CONTACTS, null);
	}
	
	public boolean setFirstTimeContacts(String firstTimeContactJSON){
		SharedPreferences.Editor editor = sharedPreferences.edit();
		editor.putString(SHARED_PREFERENCES_VARIABLE_FIRST_TIME_CONTACTS, firstTimeContactJSON);
		return editor.commit();
	}
	
	public boolean hasFirstTimeContacts(){
		String str = sharedPreferences.getString(SHARED_PREFERENCES_VARIABLE_FIRST_TIME_CONTACTS, null);
		if(str!=null){
			return true;
		}
		return false;
	}
	
	public int getNotificationCount(){
		return sharedPreferences.getInt(SHARED_PREFERENCES_VARIABLE_NOTIFICATION_COUNT, 0);
	}
	
	public boolean setNotificationCount(int count){
		SharedPreferences.Editor editor = sharedPreferences.edit();
		editor.putInt(SHARED_PREFERENCES_VARIABLE_NOTIFICATION_COUNT, count);
		return editor.commit();
	}
	
	
	
}
