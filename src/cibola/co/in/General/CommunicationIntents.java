/**
 *	CommunicationIntents.java
 *  Created by Sagar Patidar on 02-Sep-2014, 8:17:48 AM
 *	Copyright � 2014 by Cibola Technologies Pvt. Ltd
 *	All rights reserved.
 */

package cibola.co.in.General;

import android.content.Intent;

public class CommunicationIntents {

	public void setTransactionIntent(Intent intent, String toUsername, int amount, String comment, boolean isRequest){
		intent.putExtra(Constants.NETWORK_SERVICE_CODE, Constants.NETWORK_SERVICE_CODE_TRANSACTION);
		intent.putExtra(Constants.WEB_TRANSACTION_TO_USERNAME, toUsername);
		intent.putExtra(Constants.WEB_TRANSACTION_AMOUNT, amount);
		intent.putExtra(Constants.WEB_TRANSACTION_COMMENT, comment);
		intent.putExtra(Constants.TRANSACTION_IS_REQUESTED, isRequest);
	}
	
	public void setUpdateNameIntent(Intent intent, String firstname, String lastname){
		intent.putExtra(Constants.NETWORK_SERVICE_CODE, Constants.NETWORK_SERVICE_CODE_UPDATE_NAME);
		intent.putExtra(Constants.WEB_UPDATE_FIRST_NAME, firstname);
		intent.putExtra(Constants.WEB_UPDATE_LAST_NAME, lastname);
	}
	
	public void setUpdatePwdIntent(Intent intent, String oldPass, String newPass){
		intent.putExtra(Constants.NETWORK_SERVICE_CODE, Constants.NETWORK_SERVICE_CODE_UPDATE_PASSWORD);
		intent.putExtra(Constants.WEB_UPDATE_OLD_PWD, oldPass);
		intent.putExtra(Constants.WEB_UPDATE_NEW_PWD, newPass);
	}
	
	public void setUpdateEmailIntent(Intent intent, String email){
		intent.putExtra(Constants.NETWORK_SERVICE_CODE, Constants.NETWORK_SERVICE_CODE_UPDATE_EMAIL);
		intent.putExtra(Constants.WEB_UPDATE_EMAIL, email);
	}
	
	
}
