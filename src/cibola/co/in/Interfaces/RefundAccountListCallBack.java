/**
 *	RefundAccountListCallBack.java
 *  Created by Sagar Patidar on Oct 16, 2014, 3:57:42 AM
 *	Copyright � 2014 by Cibola Technologies Pvt. Ltd
 *	All rights reserved.
 */

package cibola.co.in.Interfaces;


public interface RefundAccountListCallBack {

	public void onClickAddAccount();
	
	public void extractAccountDetails(String accName, String accNumber, String accIFSC); 
	
}
