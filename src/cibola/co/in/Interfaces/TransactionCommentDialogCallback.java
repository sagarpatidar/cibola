/**
 *	TransactionCommentDialogCallback.java
 *  Created by Sagar Patidar on 02-Sep-2014, 9:58:43 AM
 *	Copyright � 2014 by Cibola Technologies Pvt. Ltd
 *	All rights reserved.
 */

package cibola.co.in.Interfaces;

public interface TransactionCommentDialogCallback {

	public void onCommentDialogDismiss();
}
