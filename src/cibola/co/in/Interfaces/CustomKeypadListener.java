/**
 *	CustomKeypadListener.java
 *  Created by Sagar Patidar on Oct 2, 2014, 2:28:29 AM
 *	Copyright � 2014 by Cibola Technologies Pvt. Ltd
 *	All rights reserved.
 */

package cibola.co.in.Interfaces;

public interface CustomKeypadListener {

	public void onEnteredValueChange(String currentValue);
}
