/**
 *	SmsReceiverCallbacks.java
 *  Created by Sagar Patidar on 27-Aug-2014, 9:28:18 PM
 *	Copyright � 2014 by Cibola Technologies Pvt. Ltd
 *	All rights reserved.
 */

package cibola.co.in.Interfaces;

public interface SmsReceiverCallbacks {

	public void receivedMessage(String message);
}
