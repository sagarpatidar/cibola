/**
 *	WebConnectionCallbacks.java
 *  Created by Sagar Patidar on 16-Aug-2014, 9:55:16 PM
 *	Copyright � 2014 by Cibola Technologies Pvt. Ltd
 *	All rights reserved.
 */

package cibola.co.in.Interfaces;

import cibola.co.in.NetworkManager.RequestType;

public interface WebConnectionCallbacks {

	public void onRequestComplete(RequestType requestType, String response);

	public void onRequestError(RequestType requestType, String errorResponse);
}
