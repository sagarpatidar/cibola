/**
 *	ContactsManager.java
 *  Created by Sagar Patidar on 04-Aug-2014, 4:21:15 PM
 *	Copyright � 2014 by Cibola Pvt. Ltd
 *	All rights reserved.
 */

package cibola.co.in.others;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.DatabaseUtils.InsertHelper;
import android.provider.ContactsContract.CommonDataKinds.Email;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.provider.ContactsContract.Data;
import android.util.Log;
import cibola.co.in.General.Constants;
import cibola.co.in.General.MyAccountManager;
import cibola.co.in.Graphics.AlphabetImage;
import cibola.co.in.NetworkManager.ImageDownloadObject;
import cibola.co.in.NetworkManager.ImageDownloader;
import cibola.co.in.SqliteManager.ContactsAtomicUpdate;
import cibola.co.in.SqliteManager.ContactsCursorAndHashMap;
import cibola.co.in.SqliteManager.Database;
import cibola.co.in.SqliteManager.DatabaseManager;
import cibola.co.in.SqliteManager.Tables;

public class ContactsManager {

	// Contants for importing contacts for default contact list
	/*
	private final Uri PHONE_CONTACTS_URI = ContactsContract.Contacts.CONTENT_URI;
	private final Uri PHONE_PHONENUMBER_URI = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
	private final Uri EmailCONTENT_URI =  ContactsContract.CommonDataKinds.Email.CONTENT_URI;

	private final String CONTACT_ID = ContactsContract.Contacts._ID;
	private final String CONTACT_DISPLAY_NAME = ContactsContract.Contacts.DISPLAY_NAME;
	private final String CONTACT_HAS_PHONE_NUMBER = ContactsContract.Contacts.HAS_PHONE_NUMBER;
	private final String PHONE_CONTACT_ID = ContactsContract.CommonDataKinds.Phone.CONTACT_ID;
	private final String PHONE_NUMBER = ContactsContract.CommonDataKinds.Phone.NUMBER;
	private final String EMAIL_CONTACT_ID = ContactsContract.CommonDataKinds.Email.CONTACT_ID;
	private final String DATA = ContactsContract.CommonDataKinds.Email.DATA;
	 */

	private ContactsAtomicUpdate atomic;
	private Cursor localContactsCursor;
	private HashMap<String, Integer> hashMap;
	private HashMap<String, Boolean> checkedContacts ;
	private HashMap<String, Boolean> checkedPhoneContacts ;
	private Context mContext;
	private Database db;
	private MyAccountManager accountManager;


	public ContactsManager(Context context){
		this.accountManager = new MyAccountManager(context);
		db = new Database(context);
		mContext = context;
	}

	private JSONObject getJSONObject(String contact_name, String mobile, String email) throws JSONException{
		JSONObject jObj = new JSONObject();
		jObj.put(Constants.WEB_SYNC_CONTACTS_NAME, contact_name);
		jObj.put(Constants.WEB_SYNC_CONTACTS_MOBILE, mobile);
		jObj.put(Constants.WEB_SYNC_CONTACTS_EMAIL, email);
		jObj.put(Constants.WEB_SYNC_CONTACTS_ACK, 1);
		jObj.put(Constants.WEB_SYNC_CONTACTS_FILE_ID, -1);
		return jObj;
	}

	public String getInitContactList(Context context) throws JSONException{
		DatabaseManager manager = new DatabaseManager(context);

		db.removeAllContacts();
		InsertHelper insertHelper = new InsertHelper(manager.getDatabase(), Tables.TABLE_CONTACT);
		checkedPhoneContacts = new HashMap<String,Boolean>();
		/*
		 * Reading contacts from phone contact's
		 */
		ContentResolver resolver = context.getContentResolver();
		Cursor c = resolver.query(
				Data.CONTENT_URI, 
				new String[]{Data.CONTACT_ID, Data.MIMETYPE, Data.DISPLAY_NAME, Data.DATA1}, 
				Data.HAS_PHONE_NUMBER + "!=0 AND (" + Data.MIMETYPE + "=? OR " + Data.MIMETYPE + "=?)", 
				new String[]{Email.CONTENT_ITEM_TYPE, Phone.CONTENT_ITEM_TYPE},
				Data.CONTACT_ID);

		String prevID = "-1";
		String email = "";
		String contact_name = "";
		List<String> mobile_numbers = new ArrayList<String>();
		JSONArray jArray = new JSONArray();
		try{
			while (c.moveToNext()) {
				String id = c.getString(c.getColumnIndex(Data.CONTACT_ID));
				String name = c.getString(c.getColumnIndex(Data.DISPLAY_NAME));
				String data1 = c.getString(c.getColumnIndex(Data.DATA1));
				String meme = c.getString(c.getColumnIndex(Data.MIMETYPE));

				if( !id.equals(prevID) && !prevID.equals("-1")){
					for(String mobile:mobile_numbers){
						Log.d("#DEBUG#1",mobile+" list length:"+mobile_numbers.size());
						jArray.put(getJSONObject(contact_name, mobile, email));
						insertHelper.prepareForInsert();
						insertHelper.bind(insertHelper.getColumnIndex(Tables.contact_mobile), mobile);
						insertHelper.bind(insertHelper.getColumnIndex(Tables.contact_name), contact_name);
						insertHelper.bind(insertHelper.getColumnIndex(Tables.contact_email), email);
						insertHelper.bind(insertHelper.getColumnIndex(Tables.contact_background), AlphabetImage.palatte[new Random().nextInt(AlphabetImage.palatte.length)]);
						insertHelper.execute();
					}
					mobile_numbers.clear();
					contact_name = "";
					email = "";
				}
				contact_name = name;

				if(meme.equals(Email.CONTENT_ITEM_TYPE)){
					email += data1+",";
				}
				else if(meme.equals(Phone.CONTENT_ITEM_TYPE)){

					String filteredPhoneNumber = data1.replaceAll("\\D+","");
					if(filteredPhoneNumber.length()==12 && filteredPhoneNumber.substring(0, 2).equals("91")){
						filteredPhoneNumber = filteredPhoneNumber.substring(2);
					}
					else if(filteredPhoneNumber.length()==11 && filteredPhoneNumber.subSequence(0, 1).equals("0")){
						filteredPhoneNumber = filteredPhoneNumber.substring(1);
					}
					if(filteredPhoneNumber.length()==10  ){
						String mob = filteredPhoneNumber;
						Boolean isChecked = checkedPhoneContacts.get(mob);
						if(isChecked==null || !isChecked){
							checkedPhoneContacts.put(mob, true);
							if(!filteredPhoneNumber.equals(accountManager.getMobileNumber())){
								mobile_numbers.add(mob);
							}
								
						}
					}
				}
				prevID = id;
			}

			if(mobile_numbers.size()!=0){
				for(String mobile:mobile_numbers){
					if(contact_name.length()>0){
						jArray.put(getJSONObject(contact_name, mobile, email));
						insertHelper.prepareForInsert();
						insertHelper.bind(insertHelper.getColumnIndex(Tables.contact_mobile), mobile);
						insertHelper.bind(insertHelper.getColumnIndex(Tables.contact_name), contact_name);
						insertHelper.bind(insertHelper.getColumnIndex(Tables.contact_email), email);
						insertHelper.bind(insertHelper.getColumnIndex(Tables.contact_background), AlphabetImage.palatte[new Random().nextInt(AlphabetImage.palatte.length)]);
						insertHelper.execute();
					}
				}
			}
		}
		catch(Exception e){
			e.printStackTrace();
		}
		finally{
			if(insertHelper!=null){
				insertHelper.close();
			}
		}
		JSONObject jsonContactsDataToSend = new JSONObject();
		String _username = accountManager.getUsername();
		if(_username!=null){
			jsonContactsDataToSend.put(Constants.WEB_SYNC_CONTACTS_SUPERUSER, _username); // to be updated
		}
		else{
			Log.d(Constants.BUG, "Username is null? How the hell!!!");
		}
		jsonContactsDataToSend.put(Constants.WEB_SYNC_CONTACTS_CONTACTS,jArray);
		return jsonContactsDataToSend.toString();
	}

	// ~10 times faster relative to prev one.
	public String getAllNativeContactsQuick() throws JSONException{
		ContactsCursorAndHashMap localContacts = db.getAllContacts();
		hashMap = localContacts.getRowMapping();
		localContactsCursor = localContacts.getLocalContactsCursor();		
		checkedContacts = new HashMap<String,Boolean>();
		checkedPhoneContacts = new HashMap<String,Boolean>();
		atomic = new ContactsAtomicUpdate(mContext);
		JSONObject jsonContactsDataToSend = new JSONObject();

		/*
		 * Reading contacts from phone contact's 
		 */
		ContentResolver resolver = mContext.getContentResolver();
		Cursor c = resolver.query(
				Data.CONTENT_URI, 
				new String[]{Data.CONTACT_ID, Data.MIMETYPE, Data.DISPLAY_NAME, Data.DATA1}, 
				Data.HAS_PHONE_NUMBER + "!=0 AND (" + Data.MIMETYPE + "=? OR " + Data.MIMETYPE + "=?)", 
				new String[]{Email.CONTENT_ITEM_TYPE, Phone.CONTENT_ITEM_TYPE},
				Data.CONTACT_ID);

		String prevID = "-1";
		String email = "";
		String contact_name = "";
		List<String> mobile_numbers = new ArrayList<String>();
		JSONArray jArray = new JSONArray();
		while (c.moveToNext()) {
			String id = c.getString(c.getColumnIndex(Data.CONTACT_ID));
			String name = c.getString(c.getColumnIndex(Data.DISPLAY_NAME));
			String data1 = c.getString(c.getColumnIndex(Data.DATA1));
			String meme = c.getString(c.getColumnIndex(Data.MIMETYPE));
			//System.out.println(id+ " MIME="+meme + ", name=" + name + ", data1=" + data1);

			if(!id.equals(prevID) && !prevID.equals("-1")){
				for(String mobile:mobile_numbers){
					jArray.put(constructAtomicUpdateContactTable(contact_name, mobile, email));
				}
				mobile_numbers.clear();
				contact_name = "";
				email = "";
			}
			contact_name = name;

			if(meme.equals(Email.CONTENT_ITEM_TYPE)){
				email += data1+",";
			}
			else if(meme.equals(Phone.CONTENT_ITEM_TYPE)){

				String filteredPhoneNumber = data1.replaceAll("\\D+","");
				if(filteredPhoneNumber.length()==12 && filteredPhoneNumber.substring(0, 2).equals("91")){
					filteredPhoneNumber = filteredPhoneNumber.substring(2);
				}
				else if(filteredPhoneNumber.length()==11 && filteredPhoneNumber.subSequence(0, 1).equals("0")){
					filteredPhoneNumber = filteredPhoneNumber.substring(1);
				}
				if(filteredPhoneNumber.length()==10 ){
					String mob = filteredPhoneNumber;
					Boolean isChecked = checkedPhoneContacts.get(mob);
					//					Log.d("isChecked",prevID+" "+mob+" "+isChecked);
					if(isChecked==null || !isChecked){
						checkedPhoneContacts.put(mob, true);
						if(!filteredPhoneNumber.equals(accountManager.getMobileNumber())){
							mobile_numbers.add(mob);
						}
					}
				}
			}
			prevID = id;
		}

		if(mobile_numbers.size()!=0){
			for(String mobile:mobile_numbers){
				if(contact_name.length()>0){
					jArray.put(constructAtomicUpdateContactTable(contact_name, mobile, email));
				}
			}
		}

		//check for deleted contacts
		for(String key:hashMap.keySet()){
			Boolean isChecked = checkedContacts.get(key);
			if(isChecked==null){
				if(!db.isContactBlocked(key)){
					JSONObject jsonContact = new JSONObject();
					jsonContact.put(Constants.WEB_SYNC_CONTACTS_NAME, "");
					jsonContact.put(Constants.WEB_SYNC_CONTACTS_MOBILE,key);
					jsonContact.put(Constants.WEB_SYNC_CONTACTS_EMAIL, "");
					jsonContact.put(Constants.WEB_SYNC_CONTACTS_ACK, -1);
					jsonContact.put(Constants.WEB_SYNC_CONTACTS_FILE_ID, -1);
					jArray.put(jsonContact);
					atomic.addDeleteID(key);
				}
			}
		}

		//Comment it out on handling
		db.removeAllRawEntry();
		atomic.commit();
		String _username = accountManager.getUsername();
		if(_username!=null){
			jsonContactsDataToSend.put(Constants.WEB_SYNC_CONTACTS_SUPERUSER, _username); // to be updated
		}
		else{
			Log.d(Constants.BUG, "Username is null? How the hell!!!");
		}
		jsonContactsDataToSend.put(Constants.WEB_SYNC_CONTACTS_CONTACTS,jArray);
		localContactsCursor.close();
		return jsonContactsDataToSend.toString();
	}

	private JSONObject constructAtomicUpdateContactTable(String contact_name, String mobile, String email) throws JSONException{
		JSONObject jObj = new JSONObject();
		jObj.put(Constants.WEB_SYNC_CONTACTS_NAME, contact_name);
		jObj.put(Constants.WEB_SYNC_CONTACTS_MOBILE, mobile);
		jObj.put(Constants.WEB_SYNC_CONTACTS_EMAIL, email);

		Integer position =  hashMap.get(mobile);
		if(position==null){
			//new Insertion
			atomic.addInsertValue(mobile, contact_name, email);
			jObj.put(Constants.WEB_SYNC_CONTACTS_ACK, 1);
			jObj.put(Constants.WEB_SYNC_CONTACTS_FILE_ID, -1);
		}
		else{
			localContactsCursor.moveToPosition(position);
			checkedContacts.put(localContactsCursor.getString(localContactsCursor.getColumnIndex(Tables.contact_mobile)), true);
			String localName = localContactsCursor.getString(localContactsCursor.getColumnIndex(Tables.contact_name));
			String localEmail = localContactsCursor.getString(localContactsCursor.getColumnIndex(Tables.contact_email));
			jObj.put(Constants.WEB_SYNC_CONTACTS_FILE_ID, localContactsCursor.getInt(localContactsCursor.getColumnIndex(Tables.contact_image_id)));
			if(!contact_name.equals(localName) || !email.equals(localEmail) ){
				//update
				atomic.addUpdateValue(mobile, contact_name, email);
				jObj.put(Constants.WEB_SYNC_CONTACTS_ACK, 2);
			}
			else{
				// neutral
				jObj.put(Constants.WEB_SYNC_CONTACTS_ACK, 0);
			}
		}
		return jObj;
	} 



	public void handleResponse(Context context, boolean isInit, String response){
		try{
			JSONObject jobj = new JSONObject(response);
			if(jobj.getInt(Constants.WEB_RESPONSE_SUCCESS)==1){
				JSONObject data = new JSONObject(jobj.getString(Constants.WEB_RESPONSE_DATA));
				//Log.d("DEBUG data",data.toString());
				if(data!=null){
					Iterator<String> iterator = data.keys();
					String key = null;
					JSONObject tempUnit = null;
					List<ImageDownloadObject> filesToDownload = new ArrayList<ImageDownloadObject>(); 
					while(iterator.hasNext()){
						key = iterator.next();
						tempUnit = data.getJSONObject(key);
						if(!isInit)
							db.removeRawEntry(key);
						if(tempUnit.getInt(Constants.WEB_RESPONSE_ON_CIBOLA)==1){
							db.updateOnCibolaStatus(key, 1);
							if(tempUnit.has(Constants.WEB_RESPONSE_FILEID)){
								ImageDownloadObject download = new ImageDownloadObject(key, 
										Constants.IMAGE_SERVER_MED_SIZE+tempUnit.getString(Constants.WEB_RESPONSE_FILE_PATH), tempUnit.getInt(Constants.WEB_RESPONSE_FILEID));
								filesToDownload.add(download);
							}
						}
					}
					if(filesToDownload.size()>0){
						ImageDownloader downloader = new ImageDownloader(context, filesToDownload, true);
						downloader.startDownload();
					}
					else{
						Intent i1 = new Intent();
						i1.setAction(Constants.CONTACTS_UPDATE_COMPLETE_BROADCAST_ACTION);
						i1.putExtra(Constants.CONTACTS_UPDATE_COMPLETE_BROADCAST_MESSAGE, 1);
						context.sendBroadcast(i1);
					}
				}
			}
			else{
				//Common.showToast(context, "Some error occurred");
				Log.d(Constants.DEBUG,"some error occurred");
			}
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}
}



