/**
 *	NavigationElement.java
 *  Created by Sagar Patidar on 04-Aug-2014, 1:36:28 AM
 *	Copyright � 2014 by Cibola Pvt. Ltd
 *	All rights reserved.
 */

package cibola.co.in.Adapters;

public class NavigationElement {

	private String first_text;
	private String second_text;
	private int iconID;
	
	public NavigationElement(String pFirstText, String pSecondText, int pIconID){
		first_text = pFirstText;
		second_text = pSecondText;
		iconID = pIconID;
	}
	
	public String getFirstText() {
		return first_text;
	}
	public void setFirstText(String first_text) {
		this.first_text = first_text;
	}
	public String getSecondText() {
		return second_text;
	}
	public void setSecondText(String second_text) {
		this.second_text = second_text;
	}
	public int getIconID() {
		return iconID;
	}
	public void setIconID(int id) {
		this.iconID = id;
	}
	
	
}
