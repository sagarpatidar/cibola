/**
 *	PagerAdapter.java
 *  Created by Sagar Patidar on 03-Aug-2014, 6:22:22 PM
 *	Copyright � 2014 by Cibola Pvt. Ltd
 *	All rights reserved.
 */

package cibola.co.in.Adapters;

import cibola.co.in.UI.Fragments.ContactsFragment;
import cibola.co.in.UI.Fragments.TransactionFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;


public class PagerAdapter extends FragmentPagerAdapter{

	public PagerAdapter(FragmentManager fm) {
		super(fm);
		
	}
	
    @Override
    public Fragment getItem(int index) {

        switch (index) {
        case 0:
            return new TransactionFragment();
        case 1:
            return new ContactsFragment();
        }
        return null;
    }

    @Override
    public int getCount() {
        return 2;
    }

}
