/**
 *	ImageCrop.java
 *  Created by Sagar Patidar on 11-Aug-2014, 12:44:30 AM
 *	Copyright � 2014 by Cibola Pvt. Ltd
 *	All rights reserved.
 */

package cibola.co.in.Graphics;

import java.io.File;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.widget.ImageView;
import cibola.co.in.General.Constants;

public class ImageCrop {

	public Intent cropImage(){
		File directory = new File(Environment.getExternalStorageDirectory()
	            + Constants.CIBOLA_MEDIUM_IMAGE_DIR);

	    if (!directory.exists()) {
	        directory.mkdirs();
	    }
		
		File tempFile = new File(Environment.getExternalStorageDirectory() +Constants.CIBOLA_USER_PROFILE_IMAGE_FILEPATH);
        Uri tempUri = Uri.fromFile(tempFile);
        Intent pickCropImageIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        //pickCropImageIntent.setAction(Intent.ACTION_GET_CONTENT);
        pickCropImageIntent.setType("image/*");
        pickCropImageIntent.putExtra("crop", "true");
        pickCropImageIntent.putExtra("outputX", 256);
        pickCropImageIntent.putExtra("outputY", 256);
        pickCropImageIntent.putExtra("aspectX", 1);
        pickCropImageIntent.putExtra("aspectY", 1);
        pickCropImageIntent.putExtra("scale", true);
        pickCropImageIntent.putExtra(MediaStore.EXTRA_OUTPUT,tempUri);
        pickCropImageIntent.putExtra("outputFormat",
                      Bitmap.CompressFormat.PNG.toString());
        
		return pickCropImageIntent;
	}
	
	
	public static void loadImageToView(ImageView imageView, String filePath ){
		File image = new File(filePath);
		if(image.exists()){
			Bitmap bitmap = BitmapFactory.decodeFile(image.getAbsolutePath());
			
		    imageView.setImageBitmap(bitmap);
		    
		}
	}

	
}
