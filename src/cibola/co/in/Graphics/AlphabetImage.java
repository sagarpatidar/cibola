/**
 *	AlphabetImage.java
 *  Created by Sagar Patidar on 14-Aug-2014, 5:35:58 PM
 *	Copyright � 2014 by Cibola Pvt. Ltd
 *	All rights reserved.
 */

package cibola.co.in.Graphics;

import java.util.Locale;
import java.util.Random;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Color;
import android.view.Gravity;
import android.widget.TextView;
import cibola.co.in.General.CircularImageView;
import cibola.co.in.General.Common;
import cibola.co.in.General.Constants;
import cibola.co.in.UI.R;

public class AlphabetImage {

	private Random rand;
	public static int[] palatte = new int[]{
			Color.rgb(84,123,202),
			Color.rgb(247,176,53),
			Color.rgb(83,97,115),
			Color.rgb(51,182,121),
			Color.rgb(187,201,61),
			Color.rgb(186,115,188),
			Color.rgb(59,164,231)
	};
	
	public AlphabetImage(){
		rand =  new Random();
	}
	public void createCircularLetterImage(Context context,String name, CircularImageView imageView, int bgColor){
		TextView textImage = new TextView(context);
		
		textImage.layout(0, 0, 100, 100);
		if(name.length()>0){
			textImage.setText(name.charAt(0)+"");
		}
		else{
			textImage.setText("?");
		}
		textImage.setTextColor(Color.WHITE);
		textImage.setTextSize(30);
		Common.setTextViewFontStyle(Constants.FONT_ROBOTO_LIGHT,context.getAssets(), textImage);
		textImage.setGravity(Gravity.CENTER);
		textImage.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.alphabet_background));
		textImage.setBackgroundColor(bgColor);
		Bitmap bitmap = Bitmap.createBitmap(100, 100, Config.ARGB_8888);
		Canvas canvas = new Canvas(bitmap);
		textImage.draw(canvas);
		imageView.setImageBitmap(bitmap);
	}
	
	public Bitmap createSquareLetterImage(Context context,String name, int color){
		TextView textImage = new TextView(context);
		
		
		int imageSize = context.getResources().getDimensionPixelSize(R.dimen.contacts_profile_pic_size_medium);
		textImage.layout(0, 0, imageSize, imageSize);
		
		if(name.length()>0){
			textImage.setText((name.charAt(0)+"").toUpperCase(Locale.US));
		}
		else{
			textImage.setText("?");
		}
		textImage.setTextColor(Color.WHITE);
		textImage.setPadding(0, 16, 0, 0);
		textImage.setTextSize(27);
		Common.setTextViewFontStyle(Constants.FONT_ROBOTO_LIGHT,context.getAssets(), textImage);
		textImage.setGravity(Gravity.CENTER);
		
		//textImage.setBackgroundColor(palatte[rand.nextInt(palatte.length)]);
		textImage.setBackgroundColor(color);
		//textImage.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.alphabet_background));
		Bitmap bitmap = Bitmap.createBitmap(imageSize, imageSize, Config.ARGB_8888);
		//bitmap.eraseColor(color);
		Canvas canvas = new Canvas(bitmap);
		textImage.draw(canvas);
//		ImageCrop crop = new ImageCrop();
		//crop.saveFileToDisk(bitmap, name.charAt(0)+".png", CompressFormat.PNG);
		return bitmap;
		//imageView.setImageBitmap(bitmap);
	}
	
}
