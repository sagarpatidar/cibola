/**
 *	SmsReceiver.java
 *  Created by Sagar Patidar on 12-Aug-2014, 1:54:31 PM
 *	Copyright � 2014 by Cibola Pvt. Ltd
 *	All rights reserved.
 */

package cibola.co.in.BroadcastReceivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;
import cibola.co.in.General.Constants;
import cibola.co.in.General.MyAccountManager;

/*
SmsReceiver receiver = new SmsReceiver();
IntentFilter filter = new IntentFilter();
filter.addAction("android.provider.Telephony.SMS_RECEIVED");
this.registerReceiver(receiver, filter);
// unregister this receiver too
*/

public class SmsReceiver extends BroadcastReceiver
{
	private final String SMS_FROM = "CIBOLA";
	private MyAccountManager accountManager;
	
    @Override
    public void onReceive(Context context, Intent intent) 
    {
        Bundle bundle = intent.getExtras();
        accountManager = new MyAccountManager(context);
        SmsMessage[] msgs = null;
        String str = "";            
        if (bundle != null){
            //retrieving the SMS message received
            Object[] pdus = (Object[]) bundle.get("pdus");
            msgs = new SmsMessage[pdus.length];            
            for (int i=0; i<msgs.length; i++){
                msgs[i] = SmsMessage.createFromPdu((byte[])pdus[i]);
                //Log.d("RECEIVED SMS","FROM: "+msgs[i].getDisplayOriginatingAddress()+"| BODY:"+msgs[i].getMessageBody());
                if(msgs[i].getOriginatingAddress().contains("-")){
	                String from = msgs[i].getOriginatingAddress().split("-")[1];
	                if(SMS_FROM.equals(from)){
	                	str = msgs[i].getMessageBody().toString();
		            	Intent i1 = new Intent();
		            	i1.setAction(Constants.OTP_ACTION);
		            	i1.putExtra(Constants.OTP_BROADCAST_MESSAGE, str);
		            	String[] elems = str.split(" ");
		        		String _code = elems[elems.length-1];
		        		try{
		        			accountManager.setVerificationCode(Integer.parseInt(_code));
		        		}
		        		catch(Exception e){
		        			e.printStackTrace();
		        		}
		            	context.sendBroadcast(i1);
	                }
                }
            }
            //Toast.makeText(context, str, Toast.LENGTH_SHORT).show();
        }                         
    }
    
}