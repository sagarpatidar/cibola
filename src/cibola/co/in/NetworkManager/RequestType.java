/**
 *	RequestType.java
 *  Created by Sagar Patidar on 15-Aug-2014, 3:02:51 AM
 *	Copyright � 2014 by Cibola Technologies Pvt. Ltd
 *	All rights reserved.
 */

package cibola.co.in.NetworkManager;

public enum RequestType {
	
	RequestTypeTesting,
	
	RequestTypeRegisterGcmOnGoogle,
	RequestTypeUnregisterGcmOnGoogle,
	RequestTypeRegisterGcmOnCibola,
	RequestTypeUnregisterGcmOnCibola,	
	
	RequestTypeSignUpStepOne,
	RequestTypeSignUpStepTwo,
	RequestTypeSignUpStepThree,
	RequestTypeSignIn,
	
	RequestTypeInitContacts,
	RequestTypeUpdateContactsList,
	RequestTypeInviteContact,
	
	RequestTypeNewTransaction,
	RequestTypeUpdateComment,
	RequestTypeUpdateStatus,
	
	RequestTypeProfileUpdateBalance,
	RequestTypeProfileUpdatePic,
	RequestTypeProfileUpdateName,
	RequestTypeProfileUpdateEmail,
	RequestTypeProfileUpdatePassword,
	RequestTypeProfileUpdateSecurityQues,
	
}
