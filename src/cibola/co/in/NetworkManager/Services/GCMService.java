/**
 *	GCMService.java
 *  Created by Sagar Patidar on 03-Sep-2014, 12:32:57 PM
 *	Copyright � 2014 by Cibola Technologies Pvt. Ltd
 *	All rights reserved.
 */

package cibola.co.in.NetworkManager.Services;

import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.util.Log;
import cibola.co.in.General.Constants;
import cibola.co.in.General.MyAccountManager;
import cibola.co.in.Graphics.PhotoManager;
import cibola.co.in.NetworkManager.ImageType;
import cibola.co.in.SqliteManager.Database;
import cibola.co.in.UI.ChatActivity;
import cibola.co.in.UI.MainActivity;
import cibola.co.in.UI.R;

import com.google.android.gms.gcm.GoogleCloudMessaging;

public class GCMService extends IntentService{

	private PhotoManager photoManager;
	private MyAccountManager accountManager;
	private final String NOTIFICATION_DELETED_ACTION = "NOTIFICATION_DELETED";

	private Database db;
	public GCMService(String name) {
		super(name);
	}
	
	public GCMService(){
		super("GCMService");
	}

	@Override
    public void onCreate() {
        super.onCreate();
        photoManager = new PhotoManager();
        db = new Database(GCMService.this);
        accountManager = new MyAccountManager(getApplicationContext());
    }
	
	@Override
	protected void onHandleIntent(Intent intent) {
		if(intent.getAction()==NOTIFICATION_DELETED_ACTION){
			accountManager.setNotificationCount(0);
			System.out.println("nailed it");
			return;
		}
		Bundle extras = intent.getExtras();
		
        GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);
        String messageType = gcm.getMessageType(intent);
        Log.i("GCM", "Received :( "+messageType+" )");
        generateNotification(getApplicationContext(), extras);
	}
	    
    
	private void generateNotification(Context context, Bundle extras){
		NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context)
												.setSmallIcon(R.drawable.ic_launcher);
		
		mBuilder.setSound(Settings.System.DEFAULT_NOTIFICATION_URI);
		mBuilder.setAutoCancel(true);
		TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
		int code = Integer.parseInt(extras.getString(Constants.NOTIFY_CODE));
		Log.d("debug: GCM Service",code+"");
		switch (code) {
			case Constants.NOTIFY_TXN_CODE:
			{
				
				String _mobile = extras.getString(Constants.NOTIFY_TXN_MOBILE);
				String _comment = extras.getString(Constants.NOTIFY_TXN_COMMENT);
				String _tid = extras.getString(Constants.NOTIFY_TXN_ID);
				int _amount = Integer.parseInt(extras.getString(Constants.NOTIFY_TXN_AMOUNT));
				int _is_requested = Integer.parseInt(extras.getString(Constants.NOTIFY_TXN_IS_REQUESTED));
				String _name = db.getName(_mobile);
				if(_name!=null){
					mBuilder.setContentTitle(_name);
					mBuilder.setLargeIcon(photoManager.getCibolaUserPic(GCMService.this, extras.getString("mobile"), _name, ImageType.Medium));
				}
				else{
					db.addContact(_mobile, _mobile, null, 1);
					_name = _mobile;
					mBuilder.setContentTitle(_mobile);
					mBuilder.setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.ic_launcher));
				}
				String body = "";
				if(_is_requested==1){
					body += "Requested Rs. "+_amount;
				}
				else{
					body += "Sent Rs. "+_amount;
					int bal = accountManager.getBalance();
					accountManager.setBalance(bal+_amount);
				}
				if(_comment!=null){
					if(_comment.length()>0){
						body += (" | "+_comment);
						db.addNewTransaction(_tid, _mobile, 0, _is_requested, _amount, _comment, 0);
					}
					else{
						db.addNewTransaction(_tid, _mobile, 0, _is_requested, _amount, null, 0);
					}
				}
				mBuilder.setContentText(body);
				int _count = accountManager.getNotificationCount();
				accountManager.setNotificationCount(++_count);
				Log.d("GCMService Debug: ", "Entering: "+_count);
				if(_count>1){
					Log.d("GCMService Debug: ", "Entering");
					mBuilder.setContentTitle("Cibola Notification")
							.setContentText("You've received new notification.")
							.setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.ic_launcher))
							.setNumber(accountManager.getNotificationCount());
					
					stackBuilder.addParentStack(MainActivity.class);
					Intent resultIntent = new Intent(this, MainActivity.class);
					resultIntent.putExtra("notification", true);
					stackBuilder.addNextIntent(resultIntent);
				}
				else{
					stackBuilder.addParentStack(ChatActivity.class);
					Intent resultIntent = new Intent(this, ChatActivity.class);
					resultIntent.putExtra(Constants.CONTACT_NAME, _name);
					resultIntent.putExtra(Constants.CONTACT_ID, _mobile);
					resultIntent.putExtra(Constants.CONTACT_BACKGROUND, db.getBackgroundColor(_mobile));
					resultIntent.putExtra("notification", true);
					stackBuilder.addNextIntent(resultIntent);
				}
				
				 Intent intent = new Intent(getApplicationContext(),GCMService.class);
				 //intent.putExtra(Constants.NOTIFICATION_CLEAR, true);
				 intent.setAction(NOTIFICATION_DELETED_ACTION);
			     //PendingIntent clearIntent = PendingIntent.getBroadcast(getApplicationContext(), 0, intent, 0);
			     PendingIntent clearIntent = PendingIntent.getService(getApplicationContext(), 0, intent, 0);
			     //registerReceiver(clearReceiver, new IntentFilter(NOTIFICATION_DELETED_ACTION));
			     mBuilder.setDeleteIntent(clearIntent); 
				break;
			}
			default:
				break;
		}

		PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(
		            						0, PendingIntent.FLAG_UPDATE_CURRENT );
		mBuilder.setContentIntent(resultPendingIntent);
		
		NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
		mNotificationManager.notify(Constants.NOTIFICATION_LOCAL_CODE, mBuilder.build());
	}
}
