/**
 *	ImageSize.java
 *  Created by Sagar Patidar on Sep 7, 2014, 3:14:16 PM
 *	Copyright � 2014 by Cibola Technologies Pvt. Ltd
 *	All rights reserved.
 */

package cibola.co.in.NetworkManager;

public enum ImageType {

	Large,// large size pics of contacts
	Medium, // Medium size pics of contacts
	Self,// Full Size
}
